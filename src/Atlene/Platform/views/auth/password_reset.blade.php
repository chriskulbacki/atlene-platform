<div class='password-reset-container login-box'>
    @if (Session::has("error"))
        <div class='login-error alert alert-danger'>{{ Session::get("error") }}</div>
    @endif

    <p>{{ tr("Type your email address and the new password.") }}</p>

    <form method='POST' action='{{ $action }}' accept-charset='UTF-8'>

        {!! csrf_field() !!}
        {!! Form::hidden('token', $token) !!}

        <div class='form-group{{ $errors->has("email") ? " has-error" : "" }}'>
            {!! Form::label("email-input", tr("E-mail")) !!}
            {!! Form::email("email", null, ["class" => "form-control", "id" => "email-input", "autofocus" => "autofocus"]) !!}
            @if ($errors->has("email"))
            <div class='error'>{{ $errors->first("email") }}</div>
            @endif
        </div>
        <div class='form-group{{ $errors->has("password") ? " has-error" : "" }}'>
            {!! Form::label("password-input", tr("Password")) !!}
            {!! Form::password("password", ["class" => "form-control", "id" => "password-input"]) !!}
            @if ($errors->has("password"))
            <div class='error'>{{ $errors->first("password") }}</div>
            @endif
        </div>
        <div class='form-group'>
            {!! Form::label("password-confirmation-input", tr("Confirm password")) !!}
            {!! Form::password("password_confirmation", ["class" => "form-control", "id" => "password-confirmation-input"]) !!}
        </div>
        <div class='form-group buttons'>
            <button type='submit' class='btn btn-primary'>{{ tr("Reset password") }}</button>
        </div>

    </form>
</div>
