<script>
    var m = moment();
    var formats = ["YYYY-MM-DD", "DD/MM/YYYY", "MM/DD/YYYY"];
    var text = "";

    for (var i = 0; i < formats.length; i++) {
        text +=
            "<a class='dt-format-sample' href='javascript:void(0)' onclick='$(\"#date_format\").val(\"" + formats[i] + "\")'>" +
                m.format(formats[i]) +
            "</a>"
    }

    $("#row-date_format_links .input-custom").append(text);
</script>