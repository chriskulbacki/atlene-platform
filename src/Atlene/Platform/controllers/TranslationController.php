<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Input;

class TranslationController extends BackendController
{
    public function index()
    {
        if (Input::get("pot")) {
            $this->createPot();
        }

        return $this->view(
            "platform::backend.translation",
            [
                "downloadPotUrl" => Q::addVarsToUrl(["pot" => 1]),
            ]
        );
    }

    public function createPot()
    {
        $temp = tmpfile();
        $target = stream_get_meta_data($temp)['uri'];

        foreach (Setting::get("translation_source_dir") as $dir) {

            $files = new \RegexIterator(
                new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir)),
                '/^.+\.php$/i',
                \RecursiveRegexIterator::GET_MATCH
            );

            foreach ($files as $array) {
                $file = $array[0];
                $blade = false;

                if (substr($file, -9) == "blade.php") {
                    $text = file_get_contents($file);
                    $text = str_replace(["{{", "}}"], ["<?php ", " ?>"], $text);
                    $file = trim(substr($file, strlen(base_path())), "/\\");
                    $file = sys_get_temp_dir() . "/" . str_replace(["/", "\\"], "_", $file);
                    file_put_contents($file, $text);
                    $blade = true;
                }

                exec("xgettext --from-code=UTF-8 -j -kt_ -c/ -L php -o '$target' '$file'");

                if ($blade) {
                    unlink($file);
                }
            }
        }

        $string = file_get_contents($target);

        header('Content-Disposition: attachment; filename="app.pot"');
        header('Content-Type: text/plain');
        header('Content-Length: ' . strlen($string));
        header('Connection: close');
        exit($string);
    }

}