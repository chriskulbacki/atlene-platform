<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Atlene\Platform\Override;
use Atlene\Platform\Crud;
use Atlene\Platform\Backend;

// authentication

Route::get(
    override("login"),
    ["as" => "login", "uses" => override("Atlene\Platform\AuthController") . "@getLogin"]
);

Route::post(
    override("login"),
    ["as" => "login.post", "uses" => override("Atlene\Platform\AuthController") . "@postLogin"]
);

Route::get(
    override("logout"),
    ["as" => "logout", "uses" => override("Atlene\Platform\AuthController") . "@getLogout"]
);

Route::get(
    override("password/email"),
    ["as" => "password.email", "uses" => override("Atlene\Platform\PasswordController") . "@getEmail"]
);

Route::post(
    override("password/email"),
    ["as" => "password.email.post", "uses" => override("Atlene\Platform\PasswordController") . "@postEmail"]
);

Route::get(
    override("password/reset/{token}"),
    ["as" => "password.reset", "uses" => override("Atlene\Platform\PasswordController") . "@getReset"]
);

Route::post(
    override("password/reset"),
    ["as" => "password.reset.post", "uses" => override("Atlene\Platform\PasswordController") . "@postReset"]
);

// keep session alive to prevent csrf errors on forms after session expired and the token no longer matches
// there's an js interval set up on all atlene pages that sends an ajax request to this route every 5 minutes

Route::get("atlene/ksa", ["as" => "atlene.ksa", function() { return ""; }] );

// image cdn

Route::get(
    "atlene/{param}/{path}",
    ["as" => "cdn.image", "uses" => "Atlene\Platform\Cdn@createImage"]
)->where("path", "(.*)");

// backend routes
// even though "admin" is overriden, it should be the same for all locales, it must not be gettext-translated
// or changing languages in the backend will not work properly (we wouldn't know all the translated versions of "admin"
// when we create the language switching menu)

Route::group(
    ["prefix" => override("admin")],
    function() {

        Backend::setVisible(true);

        Route::get(
            "/",
            [
                "as" => "admin.dashboard",
                "uses" => override("Atlene\Platform\DashboardController") . "@index",
            ]
        );

        Route::get(
            override("preferences/{id}/edit"),
            [
                "as" => "admin.preferences.edit",
                "uses" => override("Atlene\Platform\PreferencesController") . "@edit",
            ]
        );

        Route::put(
            override("preferences/{id}"),
            [
                "as" => "admin.preferences.update",
                "uses" => override("Atlene\Platform\PreferencesController") . "@update",
            ]
        );

        Route::post(
            override("preferences/color"),
            [
                "as" => "admin.preferences.color",
                "uses" => override("Atlene\Platform\PreferencesController") . "@saveColor",
            ]
        );

        Route::get(
            override("translation"),
            [
                "as" => "admin.translation",
                "uses" => override("Atlene\Platform\TranslationController") . "@index",
            ]
        );

        Crud::addRoute(override("users"), override("Atlene\Platform\AccountController"));
        Crud::addRoute(override("groups"), override("Atlene\Platform\GroupController"));

        Route::any("{catchall}", override("Atlene\Platform\BackendController") . "@getPage404")->where("catchall", "(.*)");
    }
);


