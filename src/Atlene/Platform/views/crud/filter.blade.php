<div class="crud-filter-container dialog {{ isset($code['filter-class']) ? $code['filter-class'] : '' }}">
    <form method='GET' action='' accept-charset='UTF-8'>
        <div class='dialog-title bg-primary'>{{ tr("Search") }}</div>
        <div class='dialog-content'>
            <input type='hidden' name='cftr' value='1' />
            @foreach ($fields as $field => $properties)
                @if ($properties['filter']['type'])
                    <div class='form-group'>
                        <label>{{ $properties['title']['list'] }}</label>
                        @if ($properties['filter']['type'] == "text")
                            {!! Form::text($field, Input::get($field), $properties['filter']['attr']) !!}
                        @elseif ($properties['filter']['type'] == "select")
                            {!! Form::select($field, $properties['filter']['options'], Input::get($field), $properties['filter']['attr']) !!}
                        @endif
                    </div>
                @endif
            @endforeach
        </div>
        <div class='dialog-buttons'>
            <a href='{{ $filterResetUrl }}' class='btn btn-sm btn-default'>{{ tr("Reset") }}</a>
            <button type='submit' class='btn btn-sm btn-primary'>{{ tr("Search") }}</button>
        </div>
    </form>
</div>