<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail()
    {
        return $this->view(
            override("platform::auth.password_get_email"),
            [
                "title" => tr("Reset password"),
                "action" => Site::route("password.email.post"),
                "loginRoute" => Site::route("login"),
            ]
        );
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
            $message->subject(tr("Password reminder"));
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with(
                    "success",
                    tr("Password reset instructions were sent to your email address.")
                );

            case Password::INVALID_USER:
                return redirect()->back()->with('error', tr("The email address you provided is invalid."));
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return $this->view(
            override("platform::auth.password_reset"),
            [
                "title" => tr("Reset password"),
                "action" => Site::route("password.reset.post"),
                "token" => $token,
            ]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $controllerName = override("\Atlene\Platform\AccountController");

        $this->validate(
            $request,
            [
                'token' => 'required',
                'email' => 'required|email',
                'password' => with(new $controllerName)->getCompletedFields()['password']['rules']['update'],
            ],
            Strings::validation()
        );

        $credentials = $request->only('email', 'password', 'password_confirmation', 'token');

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect()->to(Site::route("login"))->with("message", tr("Password has been reset."));

            case Password::INVALID_USER:
                return redirect()->back()->with("error", tr("The specified user account doesn't exist."));

            case Password::INVALID_TOKEN:
                return redirect()->back()->with("error", tr("The password reset token is invalid."));

            default:
                return redirect()->back()->with("error", $response);
        }
    }
}
