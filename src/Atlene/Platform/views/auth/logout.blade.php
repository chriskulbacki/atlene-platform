<div class='logout-container login-box'>
    <p class='logout-message'>{{ tr("You have been successfully logged out.") }}</p>
    <p class='buttons'>
        <a href='{{ $loginRoute }}' class='btn btn-primary'>{{ tr("Log in again") }}</a>
        <a href='{{ $siteRoute }}' class='btn btn-success'>{{ tr("Browse website") }}</a>
    </p>
</div>
