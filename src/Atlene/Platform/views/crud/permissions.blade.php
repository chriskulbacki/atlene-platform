<div class='permissions-all-actions'>
    <button type="button" class="btn btn-default btn-xs" onclick="access.setAll(2)">{{ tr("Allow all") }}</button>
    @if ($showInherited)
        <button type="button" class="btn btn-default btn-xs" onclick="access.setAll(1)">{{ tr("Inherit all") }}</button>
    @endif
    <button type="button" class="btn btn-default btn-xs" onclick="access.setAll(0)">{{ tr("Deny all") }}</button>
</div>

<input type="hidden" name="permissions" id="permissions" value='{{$value}}'/>
<table class='table table-condensed table-bordered permissions-table'>
    @foreach ($permissions as $category => $items)
        <tr class='active'>
            <th colspan='3'>{{ $category }}</th>
        </tr>

        @foreach ($items as $item)
        <tr id="row-{{$item['key']}}" class="permission-{{$item['value']}} {{$item['class']}}">
            <td class='input'>
                <div class="btn-group" data-toggle="buttons">
                    @foreach ($buttons as $const => $button)
                        <?php if ($const == 1 && !$showInherited) { continue; } ?>
                        <label class='btn btn-sm btn-default{{ $item['value'] == $const ? " active" : "" }}' title="{{ $button['title'] }}">
                            {!! Form::radio($item['key'], $const, $item['value'] == $const) !!}
                            <i class="fa fa-{{ $button['icon'] }}"></i>
                        </label>
                    @endforeach
                </div>
            </td>
            <td class='title'>
                <div>{{ $item['name'] }}</div>
                <div class='small text-muted'>{{ $item['description'] }}</div>
            </td>
            <td>{{ $item['key'] }}</td>
        </tr>
        @endforeach
    @endforeach
</table>

<script>
    $(".permissions-table input[type=radio]").change(function() { access.change(this) });
</script>
