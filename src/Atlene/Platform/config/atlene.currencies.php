<?php

return [

    1 => [
        "code" => "USD",
        "symbol" => "$",
        "name" => _("U.S. Dollar"),
        'exchange_precision' => 0.2,
    ],

];
