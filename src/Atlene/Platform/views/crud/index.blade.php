<div id='crud-index-{{$crudId}}' class='crud-index crud'>

    {!! isset($code['above-buttons']) ? $code['above-buttons'] : "" !!}

    @if ($topButtons)
        <div class='top-buttons'>{!! $topButtons !!}</div>
    @endif

    {!! isset($code['below-buttons']) ? $code['below-buttons'] : "" !!}

    @if ($hasFilters)
        @include($views['filter'])
    @endif

    {!! isset($code['below-filters']) ? $code['below-filters'] : "" !!}

    <table class="{{ isset($code['table-class']) ? $code['table-class'] : "" }}">
        <thead>
            <tr>
                @foreach ($fields as $field => $properties)
                    @if ($properties['list']['display'])
                        <th>
                            @if ($properties['list']['sort'])
                                <?php
                                    $sortValues = ["sort" => $field, "desc" => 0];
                                    $mark = false;

                                    if ($sort == $field) {
                                        if (!$desc) {
                                            $sortValues["desc"] = 1;
                                        }
                                        $mark = $sortValues['desc'] ? "up" : "down";
                                    }

                                    $url = Atlene\Platform\Q::addVarsToUrl($sortValues);
                                ?>
                                <a href='{{ $url }}'>
                                {{ $properties['title']['list'] }}
                                @if ($mark)
                                    <i class="fa fa-caret-{{$mark}}"></i>
                                @endif
                                </a>
                            @else
                                {{ $properties['title']['list'] }}
                            @endif
                        </th>
                    @endif
                @endforeach
                @if ($showListButtonColumn)
                    <th class='actions'></th>
                @endif
            </tr>
        </thead>

        <tbody>
            @foreach ($items as $item)
                <tr id='item-{{$item->id}}' class="{{ isset($code['row-class']) ? $code['row-class'] : "" }}">
                    @foreach ($fields as $field => $properties)
                        @if ($properties['list']['display'])
                            <td>{!! $item->$field !!}</td>
                        @endif
                    @endforeach
                    @if ($showListButtonColumn)
                        <td class='actions'>{!! $item->buttons !!}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    {!! isset($code['above-links']) ? $code['above-links'] : "" !!}

    {!! $paginator !!}

    {!! isset($code['below-links']) ? $code['below-links'] : "" !!}

    @if (Session::has("message"))
        <script>
            $(document).ready(function() {
                notification.show("{{{ Session::get('message') }}}");
            });
        </script>
    @endif
</div>