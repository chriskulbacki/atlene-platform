<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class ResourceCode
{
    private $resources = [
        "countries" => null,
        "locales" => null,
        "languages" => null,
    ];

    private $countryCodes = [];

    public function getCountries()
    {
        return $this->load("countries");
    }

    public function getCountryNameByCode($code)
    {
        $this->load("countries");

        $code = strtoupper($code);

        if (array_key_exists($code, $this->countryCodes)) {
            return $this->countryCodes[$code][1];
        }

        return false;
    }

    public function isValidCountryCode($code)
    {
        $code = strtoupper($code);

        foreach ($this->getCountries() as $item) {
            if ($item[0] == $code) {
                return true;
            }
        }

        return false;
    }

    public function getLocales()
    {
        return $this->load("locales");
    }

    public function isValidLocale($code)
    {
        foreach ($this->getLocales() as $item) {
            if (strcasecmp($item[0], $code) == 0 || strcasecmp($item[1], $code) == 0) {
                return true;
            }
        }

        return false;
    }

    public function getLanguages()
    {
        return $this->load("languages");
    }

    /**
     * Returns the array in the format: key => "english_name (local_name)"
     * @return Ambigous <string, multitype:NULL >
     */
    public function getLanguageSelectData($includeLocalNames = true)
    {
        $data = $this->getLanguages();

        foreach ($data as $key => $names) {
            $data[$key] = $names[0] . ($includeLocalNames ? " ({$names[1]})" : "");
        }

        return $data;
    }

    private function load($resource)
    {
        if (!$this->resources[$resource]) {
            $code = Locale::get("code");
            $dir = __DIR__ ."/../resources/$resource/";

            if (file_exists($dir . $resource . "_$code.php")) {
                $this->resources[$resource] = include($dir . $resource . "_$code.php");
            } else {
                $this->resources[$resource] = include($dir . $resource . "_en.php");
            }

            // invert country array so the country codes are the keys - for faster lookup
            if ($resource == "countries") {
                $this->countryCodes = [];
                foreach ($this->getCountries() as $key => $item) {
                    $this->countryCodes[$item[0]] = [$key, $item[1], $item[2]];
                }
            }
        }

        return $this->resources[$resource];
    }

}



