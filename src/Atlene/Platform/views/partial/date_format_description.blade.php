<table class='table table-condensed'>
    <tr>
        <th colspan='2'>{{ tr("Day") }}</th>
        <th colspan='2'>{{ tr("Month") }}</th>
        <th colspan='2'>{{ tr("Year") }}</th>
    </tr>
    <tr>
        <td>D</td>
        <td>1 2 3</td>
        <td>M</td>
        <td>1 2 3</td>
        <td>YYYY</td>
        <td>{{ date("Y") }} {{ date("Y") + 1 }}...</td>
    </tr>
    <tr>
        <td>DD</td>
        <td>01 02 03</td>
        <td>MM</td>
        <td>01 02 03</td>
        <td>YY</td>
        <td>{{ date("y") }} {{ date("y") + 1 }}...</td>
    </tr>
    <tr>
        <td>ddd</td>
        <td>{{ date("D") }} {{ date("D", strtotime("+1 day")) }}</td>
        <td>MMM</td>
        <td>{{ date("M") }} {{ date("M", strtotime("+1 month")) }}</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>dddd</td>
        <td>{{ date("l") }} {{ date("l", strtotime("+1 day")) }}</td>
        <td>MMMM</td>
        <td>{{ date("F") }} {{ date("F", strtotime("+1 month")) }}</td>
        <td></td>
        <td></td>
    </tr>
</table>