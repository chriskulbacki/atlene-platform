<div id='error-page'>
    <img src='{{ asset("atlene/platform/images/error.png") }}' alt='' />
    <p class='error-message'>{{ $message }}</p>
</div>
