<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Atlene\Platform\User;
use Atlene\Platform\Group;

class AtlenePlatformSetup extends Migration {

    public function up()
    {
        if (Schema::hasTable("users") && !Schema::hasColumn("users", "permissions")) {
            Schema::drop("users");
        }

        if (Schema::hasTable("groups")) {
            Schema::drop("groups");
        }

        if (Schema::hasTable("users_groups")) {
            Schema::drop("users_groups");
        }

        if (Schema::hasTable("password_resets")) {
            Schema::drop("password_resets");
        }

        Schema::create("users", function(Blueprint $table) {
            $table->increments("id");
            $table->string("email", 100)->unique();
            $table->string("password", 70);
            $table->tinyInteger("superuser")->nullable();
            $table->text("permissions")->nullable();

            $table->string("first_name")->nullable();
            $table->string("last_name")->nullable();
            $table->enum("sex", ["male", "female", "other"]);
            $table->date("birth_date")->nullable();
            $table->string("avatar", 255)->nullable();
            $table->string("date_format");
            $table->string("time_format");
            $table->string("backend_color", 255);
            $table->tinyInteger("backend_gradient")->default(1);

            $table->timestamp("last_password_change")->nullable();
            $table->text("previous_passwords")->nullable();
            $table->timestamp("last_login")->nullable();
            $table->string("last_ip")->nullable();

            $table->string("activation_code")->nullable()->index();
            $table->timestamp("activated_at")->nullable();
            $table->timestamp("suspended_at")->nullable();

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->engine = "InnoDB";
        });

        Schema::create("groups", function(Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->text("permissions")->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->engine = "InnoDB";
            $table->unique("name");
            $table->engine = "InnoDB";
        });

        Schema::create("users_groups", function(Blueprint $table) {
            $table->integer("user_id")->unsigned();
            $table->integer("group_id")->unsigned();

            $table->primary(["user_id", "group_id"]);
            $table->engine = "InnoDB";
        });

        Schema::create('password_resets', function(Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });

        $group = new Group();
        $group->id = 1;
        $group->name = "user";
        $group->save();

        $user = new User();
        $user->id = 1;
        $user->email = "admin@admin.com";
        $user->password = bcrypt("admin");
        $user->superuser = 1;
        $user->first_name = "Admin";
        $user->activated_at = date("Y:m:d H:i:s");
        $user->last_password_change = date("Y:m:d H:i:s");
        $user->save();
    }

    public function down()
    {
        Schema::drop("users");
        Schema::drop("groups");
        Schema::drop("users_groups");
        Schema::drop('password_resets');
    }

}
