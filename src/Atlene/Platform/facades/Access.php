<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Support\Facades\Facade;

class Access extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "accesscode";
    }
}

