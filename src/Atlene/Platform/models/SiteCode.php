<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Request;
use Route;

class SiteCode extends \Atlene\Platform\Configuration
{
    protected $name = "atlene.sites";

    public function setCurrent()
    {
        parent::setCurrent();
        $domain = Request::server('SERVER_NAME', false);

        foreach ($this->config as $id => $item) {
            if (in_array($domain, $item['domains'])) {
                $this->id = $id;
                break;
            }
        }

        // if not found, set it to the first id
        $this->id || $this->id = 1;
    }

    public function fixConfig()
    {
        $this->removeNonNumericKeys();

        foreach ($this->config as $key => $options) {
            array_key_exists("domains", $options) || $this->config[$key]['domains'] = ["localhost"];
            array_key_exists("title", $options) || $this->config[$key]['title'] = tr("Website");
            array_key_exists("country_name", $options) || $this->config[$key]['country_name'] = tr("United States");
            array_key_exists("locale_code", $options) || $this->config[$key]['locale_code'] = "en";
            array_key_exists("date_format", $options) || $this->config[$key]['date_format'] = "YYYY-MM-DD";
            array_key_exists("time_format", $options) || $this->config[$key]['time_format'] = "HH:mm";
            array_key_exists("main_currency_code", $options) || $this->config[$key]['main_currency_code'] = "USD";
            array_key_exists("enabled_currency_codes", $options) ||
                $this->config[$key]['enabled_currency_codes'] = "all";
            array_key_exists("layout", $options) || $this->config[$key]['layout'] = "default";

            is_array($options['domains']) || $this->config[$key]['domains'] = [$options['domains']];
        }
    }

    /**
     * Returns a url for the named route including the language code in the url if the language of the current site
     * doesn't equal the current language.
     * This function should be used instead of URL::route() and route().
     * */
    public function route($name, $params = [])
    {
        if (Route::has($name)) {
            return $this->url(route($name, $params));
        }

        return "/";
    }

    /**
     * Takes a url and adds (or changes) the language component
     * For example: http://site.com/hello -> http://site.com/pl/hello
     * If the locale code of the current site is the same as the locale the site's currently running in, the function
     * removes the locale code from url. In other words, if the site locale is en, the url will never be
     * http://site.com/en/something - it'll be http://site.com/something
     */
    public function url($path = false, $localeCode = false)
    {
        $localeCode || $localeCode = Locale::get("code");

        // remove domain name from $path
        $path = ltrim(str_replace(url(), "", $path), "/");

        // remove a locale code from the beginning of the path, if it exists
        $prefix = explode("/", $path)[0];
        if (Resource::isValidLocale($prefix)) {
            $path = substr($path, strlen($prefix) + 1);
        }

        // get the url locale that should be used with this site and locale and prepend it to path
        if ($urlLocaleCode = $this->getUrlLocaleCode($localeCode)) {
            $path = $urlLocaleCode . "/" . $path;
        }

        return url($path);
    }

    /**
     * Returns the locale code that should be added to the url to make the correct full url path for this site and
     * language. The parameter $localeCode can be used to overwrite the language currently used.
     */
    public function getUrlLocaleCode($localeCode = false)
    {
        $localeCode || $localeCode = Locale::get("code");

        return Site::get("locale_code") == $localeCode ? false : $localeCode;
    }

    public function isBackend()
    {
        return true; // XXX

        if ($this->backend === null) {
            $host = $this->config['admin_subdomain'] ? explode(".", Request::server("HTTP_HOST")) : false;
            $url = explode("/", trim(Request::server("REQUEST_URI"), "/"));

            $this->backend =
                substr($url[0], 0, 5) == "admin" &&
                ($host[0] == $this->config['admin_subdomain'] || $this->isDevMode());
        }

        return $this->backend;
    }

    public function isFrontend()
    {
        return false; // XXX

        return $this->isHttpRequest() && !$this->isBackend();
    }
}
