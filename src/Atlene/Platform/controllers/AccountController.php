<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use View;

class AccountController extends CrudController
{
    protected $modelName = 'Atlene\Platform\Account';

    public function getTabs()
    {
        return [
            "user" => tr("User"),
            "avatar" => tr("Avatar"),
            "password" => tr("Password"),
            "groups" => tr("Groups"),
            "permissions" => ("Permissions"),
            "preferences" => tr("Preferences"),
        ];
    }

    public function getFields()
    {
        return [
            "id" => [
                "title" => tr("ID"),
                "list" => true,
                "filter" => "text",
            ],
            "name" => [
                "title" => tr("Name"),
                "list" => [
                    "display" => true,
                    "sort" => "first_name|last_name",
                ],
                "filter" => [
                    "type" => "text",
                    "column" => "first_name|last_name",
                ],
            ],
            "first_name" => [
                "title" => tr("First name"),
                "input" => "text",
                "rules" => "required",
                "tab" => "user",
            ],
            "last_name" => [
                "title" => tr("Last name"),
                "input" => "text",
                "rules" => "required",
                "tab" => "user",
            ],
            "email" => [
                "title" => tr("E-mail"),
                "input" => "email",
                "rules" => [
                    "create" => "required|email|unique:" . Setting::get("table_users"),
                    "update" => "required|email|unique:" . Setting::get("table_users") . ",email,~id",
                ],
                "tab" => "user",
                "list" => true,
                "filter" => "text",
            ],
            "sex" => [
                "title" => tr("Sex"),
                "input" => [
                    "type" => "select",
                    "options" => $this->getSexSelectOptions(),
                ],
                "tab" => "user",
            ],
            "birth_date" => [
                "title" => tr("Date of birth"),
                "input" => "date",
                "tab" => "user",
            ],
            "superuser" => [
                "title" => tr("Superuser"),
                "input" => "checkbox",
                "tab" => "user",
            ],
            "suspended_at" => [
                "title" => tr("Suspended"),
                "input" => "checkbox",
                "tab" => "user",
            ],
            "password" => [
                "title" => tr("Password"),
                "input" => "password",
                "rules" => [
                    "create" => "required|min:8",
                    "update" => "sometimes|required|confirmed|min:8",
                ],
                "tab" => "password",
                "errors" => [
                    "min" => tr("The password must have at least 8 characters."),
                ],
            ],
            "password_confirmation" => [
                "title" => tr("Confirm password"),
                "input" => [
                    "create" => false,
                    "update" => "password",
                ],
                "tab" => "password",
                "save" => false,
            ],
            "avatar" => [
                "title" => "",
                "input" => [
                    "type" => "image",
                    "path" => "avatars",
                    "options" => [], // http://plugins.krajee.com/file-input
                ],
                "tab" => "avatar",
            ],
            "avatar_description" => [
                "title" => "",
                "input" => "custom",
                "value" => View::make("platform::partial.avatar_description"),
                "tab" => "avatar",
                "save" => false,
            ],
            "date_format" => [
                "title" => tr("Date format"),
                "input" => "text",
                "tab" => "preferences",
            ],
            "date_format_links" => [
                "title" => "Common formats",
                "input" => "custom",
                "value" => View::make("platform::partial.date_format_links"),
                "tab" => "preferences",
                "save" => false,
            ],
            "time_format" => [
                "title" => tr("Time format"),
                "input" => "text",
                "tab" => "preferences",
            ],
            "time_format_links" => [
                "title" => "Common formats",
                "input" => "custom",
                "value" => View::make("platform::partial.time_format_links"),
                "tab" => "preferences",
                "save" => false,
            ],
            "date_format_description" => [
                "title" => tr("Date format tokens"),
                "input" => "custom",
                "value" => View::make("platform::partial.date_format_description"),
                "tab" => "preferences",
                "save" => false,
            ],
            "time_format_description" => [
                "title" => tr("Time format tokens"),
                "input" => "custom",
                "value" => View::make("platform::partial.time_format_description"),
                "tab" => "preferences",
                "save" => false,
            ],
            "last_login" => [
                "title" => tr("Last login"),
                "list" => true,
            ],
            "status" => [
                "title" => tr("Status"),
                "list" => true,
            ],
            "groups" => [
                "title" => [
                    "list" => tr("Groups"),
                    "create" => false,
                    "update" => false,
                ],
                "relation" => true,
                "input" => Override::get("Atlene\Platform\Account") . "::getGroupsInput",
                "tab" => "groups",
                "list" => [
                    "display" => true,
                    "sort" => false,
                ],
                "filter" => [
                    "type" => "select",
                    "column" => "id",
                    "title" => "name",
                    "options" => Override::get("Atlene\Platform\Group") . "::lists",
                ],
            ],
            "permissions" => [
                "title" => false,
                "input" => [
                    "type" =>  Override::get("Atlene\Platform\GroupController") . "::getPermissionsInput",
                    "inline" => false,
                    "options" => true,
                ],
                "tab" => "permissions",
            ],
        ];
    }

    private function getSexSelectOptions()
    {
        return [
            false => "- " . tr("Select") . " -",
            "female" => tr("Female"),
            "male" => tr("Male"),
            "other" => tr("Other")
        ];
    }
}