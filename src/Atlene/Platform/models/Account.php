<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use DB;
use Input;
use Session;
use Eloquent;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Account extends Crud
{
    protected $table = 'users';
    protected $hidden = ['password'];

    public function __construct()
    {
        $this->table = Setting::get("table_users");
    }

    public function groups()
    {
        return $this->belongsToMany("\Atlene\Platform\Group", Setting::get("table_users_groups"), "user_id", "group_id");
    }

    public function getGroupsAttribute()
    {
        $display = [];

        if (isset($this->relations['groups'])) {
            foreach ($this->relations['groups'] as $group) {
                 $display[] = $group->name;
            }
        }

        return implode(", ", $display);
    }

    public function getNameAttribute($value)
    {
        return $this->first_name . " " . $this->last_name;
    }

    public static function getGroupsInput($record)
    {
        $ids = $record ? $record->groups()->lists("id")->all() : [];
        $items = DB::table(Setting::get("table_groups"))->lists("name", "id");
        $controls = [];

        foreach ($items as $id => $title) {
            $controls[] = Element::div(
                Element::checkbox("users_groups[$id]", $title, in_array($id, $ids)),
                ["class" => "checkbox"]
            );
        }

        return Element::div(implode("", $controls), ["class" => "crud-checkbox-list"]);
    }

    public function getLastLoginAttribute($value)
    {
        return $value ? Backend::dateTime($value) : _("Never");
    }

    public function getStatusAttribute()
    {
        $output = [
            Element::i(
                false,
                [
                    "class" => "fa fa-check " . ($this->activated_at ? "" : " disabled text-muted"),
                    "title" => $this->activated_at ? tr("Activated") : tr("Not activated"),
                ]
            ),
            Element::i(
                false,
                [
                    "class" => "fa fa-exclamation-triangle " . ($this->suspended_at ? "" : " disabled text-muted"),
                    "title" => $this->suspended_at ? tr("Suspended") : tr("Not suspended"),
                ]
            ),
            Element::i(
                false,
                [
                    "class" => "fa fa-linux " . ($this->superuser ? "" : " disabled text-muted"),
                    "title" => $this->superuser ? tr("Superuser") : tr("Not superuser"),
                ]
            ),
        ];


        return Element::span(implode(" ", $output), ["class" => "user-status"]);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setPasswordConfirmationAttribute($value)
    {
        $this->attributes['password_confirmation'] = bcrypt($value);
    }

    public function getSuspendedAtAttribute($value)
    {
        return $value != null;
    }

    public function setSuspendedAtAttribute($value)
    {
        // don't update the suspended date if it's already suspended
        if ($this->attributes['suspended_at'] && $value) {
            return;
        }

        $this->attributes['suspended_at'] = $value ? date("Y-m-d H:i:s") : null;
    }

    public function save(array $options = [])
    {
        if (Input::has("password")) {
            $this->setPasswordAttribute(Input::get("password"));
        }

        // new accounts created manually should always be activated by default
        if (!$this->id) {
            $this->activated_at = date("Y-m-d H:i:s");
        }

        $result = parent::save($options);

        // save groups - need to do it after save() because when creating we don't have the record id until after save
        // and the id is used to store the group_id => user_id pairs
        if ($result) {
            $values = Input::get("users_groups", []);
            is_array($values) || $values = [];
            $values = array_keys($values);
            $this->groups()->sync($values);
        }

        return $result;
    }

    public static function destroy($id)
    {
        DB::table(Setting::get("table_users_groups"))->where("user_id", $id)->delete();

        parent::destroy($id);
    }

    public function createAccount($groups = [])
    {
        $result = parent::save();

        if ($result) {
            $this->groups()->sync($groups);
        }

        return $result;
    }

    public function parentSave(array $options = [])
    {
        return parent::save($options);
    }
}
