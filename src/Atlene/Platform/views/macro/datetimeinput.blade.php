<?php
    // http://eonasdan.github.io/bootstrap-datetimepicker

    $id = rand(0, 999999);

    switch ($type) {
        case "date":
            $returnFormat = "YYYY-MM-DD";
            break;
        case "time":
            $returnFormat = "HH:mm:ss";
            break;
        default:
            $returnFormat = "YYYY-MM-DD HH:mm:ss";
    }
?>

<div class='input-group datetime' id='input-{{$id}}'>
    <span class="input-group-addon datepickerbutton">
        <i class="fa fa-calendar"></i>
    </span>
    {!! Form::text($field . "-text", "", $attr) !!}
    {!! Form::hidden($field, $value) !!}
</div>

<script>
    var dtText =  $("#input-{{$id}} input[type=text]");
    var dtValue = $("#input-{{$id}} input[type=hidden]");

    dtText.datetimepicker({!! json_encode($options) !!});

    if (dtValue.val()) {
        dtText.data("DateTimePicker").date(moment(dtValue.val(), "YYYY-MM-DD"));
    }

    dtText.on("dp.change",function (e) {
        dtValue.val(moment(e.date).format('{{ $returnFormat }}'));
    });
</script>