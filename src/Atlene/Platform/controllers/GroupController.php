<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use View;

class GroupController extends CrudController
{
    protected $modelName = "Atlene\Platform\Group";

    public function getFields()
    {
        return [
            "id" => [
                "title" => tr("ID"),
                "list" => true,
            ],
            "name" => [
                "title" => tr("Name"),
                "input" => "text",
                "rules" => "required",
                "list" => true,
            ],
            "permissions" => [
                "title" => tr("Permissions"),
                "input" => [
                    "type" => "Atlene\Platform\GroupController::getPermissionsInput",
                    "inline" => false,
                    "options" => false,
                ],
            ],
        ];
    }

    public static function getPermissionsInput($record, $showInherited)
    {
        if ($record && !empty($record->permissions)) {
            $value = $record->permissions;
            $array = json_decode($record->permissions, true);
        } else {
            $value = "";
            $array = [];
        }

        is_array($array) || $array = [];

        $buttons = [
            2 => ["title" => tr("Allowed"), "icon" => "check"],
            1 => ["title" => tr("Inherited"), "icon" => "minus"],
            0 => ["title" => tr("Denied"), "icon" => "times"],
        ];

        $permissions = Access::getAllPermissions();
        foreach ($permissions as $category => $items) {
            foreach ($items as $index => $item) {

                if (!array_key_exists($item['key'], $array)) {
                    $permissions[$category][$index]['value'] = 1;
                } else {
                    $permissions[$category][$index]['value'] = $array[$item['key']];
                }

                switch ($permissions[$category][$index]['value']) {
                    case 0:
                        $permissions[$category][$index]['current'] = tr("Denied");
                        $permissions[$category][$index]['class'] = "bg-danger";
                        break;
                    case 2:
                        $permissions[$category][$index]['current'] = tr("Allowed");
                        $permissions[$category][$index]['class'] = "bg-success";
                        break;
                    default:
                        $permissions[$category][$index]['current'] = tr("Inherited");
                        $permissions[$category][$index]['class'] = "";
                }

                if (!$showInherited && $permissions[$category][$index]['value'] == 1) {
                    $permissions[$category][$index]['value'] = 0;
                    $permissions[$category][$index]['class'] = "bg-danger";
                }
            }
        }

        return View::make(
            Override::get("platform::crud.permissions"),
            [
                "permissions" => $permissions,
                "value" => $value,
                "array" => $array,
                "buttons" => $buttons,
                "showInherited" => $showInherited,
            ]
        )->render();
    }
}
