<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Request;

/**
 * A support class containing general utility functions.
 *
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2011 Chris Kulbacki
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 */

class Q
{
    public static function encodeId($id)
    {
        return base_convert(crc32(\Config::get("app.key")) + $id, 10, 36);
    }

    public static function decodeId($encodedId)
    {
        return base_convert($encodedId, 36, 10) - crc32(\Config::get("app.key"));
    }

    public static function dbTime($time = false)
    {
        if (!$time) {
            $time = time();
        }

        return date("Y-m-d H:i:s", $time);
    }

    public static function url($skipHttp = false)
    {
        $serverName = Request::server('SERVER_NAME', "localhost");
        $serverProtocol = Request::server('SERVER_PROTOCOL', "http");
        $https = Request::server('HTTPS', false);

        if (strpos(strtolower($serverName), 'www.') === 0) {
            $serverName = substr($serverName, 4);
        }

        if ($skipHttp) {
            $http = "";
        } else {
            $http = substr(
                strtolower($serverProtocol),
                0,
                strpos(strtolower($serverProtocol), "/")
            ) .
            (empty($https) ? '' : ($https == "on") ? "s" : "") . "://";
        }

        return rtrim($http . $serverName, "/");
    }

    public static function fullUrl($server = false, $stripArguments = false)
    {
        $httpHost = Request::server('HTTP_HOST', "localhost");

        $server = $server? rtrim($server, "/") : $httpHost;

        $https = Request::server('HTTPS');

        $url = ($https == 'on' ? 'https' : 'http') . "://" . $server . Request::server('REQUEST_URI');

        if ($stripArguments && $i = strpos($url, "?")) {
            $url = substr($url, 0, $i);
        }

        return $url;
    }

    public static function removeVarsFromUrl($variables, $url = false)
    {
        $url || $url = self::fullUrl();

        $queryStart = strpos($url, "?");

        if (!$variables || !$queryStart) {
            return $url;
        }

        if (!is_array($variables)) {
            $variables = array($variables);
        }

        parse_str(parse_url($url, PHP_URL_QUERY), $array);

        foreach ($variables as $val) {
            unset($array[$val]);
        }

        $query = http_build_query($array);

        return substr($url, 0, $queryStart) . ($query ? "?" . $query : "");
    }

    public static function addVarsToUrl(array $variables, $url = false)
    {
        $url || $url = self::fullUrl();

        if (!$variables) {
            return $url;
        }

        parse_str(parse_url($url, PHP_URL_QUERY), $array);

        foreach ($variables as $key => $val) {
            $array[$key] = $val;
        }

        if ($i = strpos($url, "?")) {
            $url = substr($url, 0, $i);
        }

        return $url . "?" . http_build_query($array);
    }

    public static function metaRedirect($url)
    {
        exit("<html><head><meta http-equiv='refresh' content='0;url=$url'></head></html>");
    }

    public static function javascriptRedirect($url)
    {
        exit("<script>window.location='$url'</script>");
    }

    public static function permanentRedirect($url)
    {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $url);
        exit();
    }

    public static function temporaryRedirect($url)
    {
        header("HTTP/1.1 302 Found");
        header("Location: " . $url);
        exit();
    }

    public static function backwardStrpos($haystack, $needle, $offset = 0)
    {
        $length = strlen($haystack);
        $offset = $offset > 0 ? $length - $offset : abs($offset);
        $pos = strpos(strrev($haystack), strrev($needle), $offset);
        return ($pos === false) ? false : $length - $pos - strlen($needle);
    }

    public static function link($text, $href, $onclick = false, $newWindow = false)
    {
        // replaces [a] ... [/a] with actual link

        if (!$href) {
            $href = "javascript:void(0)";
        }

        $text = str_replace(
            "[a]",
            "<a" . ($newWindow ? " target='_blank'" : "") . " href='$href'" .
            ($onclick ? " onclick='" . $onclick . "'" : "") . ">",
            $text
        );

        return str_replace("[/a]", "</a>", $text);
    }

    public static function boolToHuman($value)
    {
        return $value ? tr("Yes") : tr("No");
    }

    public static function operatingSystem()
    {
        $ua = \Request::server('HTTP_USER_AGENT');

        if (strpos($ua, "Linux")) {
            return "Linux";
        }

        if (strpos($ua, "Macintosh")) {
            return "Mac";
        }

        if (strpos($ua, "Windows")) {
            return "Windows";
        }

        return false;
    }

    public static function permalink($text)
    {
        // create permalink that can be used for page url

        $text = trim($text);

        if (!$text) {
            return false;
        }

        $text = iconv("UTF-8", "ASCII//TRANSLIT", $text);
        $text = preg_replace("%[^-/+|\w ]%", "-", $text);
        $text = strtolower(trim($text, "-"));
        $text = preg_replace("/[\/_|+ -]+/", "-", $text);

        return $text;
    }

    public static function shortenString($string, $length = 50)
    {
        $string = trim($string);

        if (strlen($string) <= $length) {
            return $string;
        }

        $string = substr($string, 0, $length);

        if ($i = strrpos($string, " ")) {
            $string = substr($string, 0, $i);
        }

        return $string . "...";
    }

    /**
     * Encrypts a string using Blowfish. Returns a 16-character string if encrypting an unsigned integer, or a
     * longer string if encrypting a string.
     *
     * @param mixed $string String or integer to encrypt.
     * @param unknown $makeUnique Whether should add a salt to the encryption and make the encrypted text unique.
     *
     * @return string
     */
    public static function encrypt($string, $password, $makeUnique = false)
    {
        // Can be used for passing ids in urls, naming images, etc.
        // Always returns 16-character string for unsigned integers, 16, 32, 48, etc. - character string
        // for longer inputs

        // make sure it's a string, because integers might encrypt differently each time, which is not good
        // if we rely on the encryption to be always the same, as in looking up encrypted file names

        $string = (string)$string;

        if ($string === "" || $string === false || !function_exists("mcrypt_encrypt")) { // allow encrypting of "0"
            return $string;
        }

        // if it's an unsigned 32-bit integer, convert it to binary to make the output as short
        // as possible (16 characters)

        if (ctype_digit($string) && $string >= 0 && $string <= 0xFFFFFFFF) {
            $string = "1" . pack("L", $string);
        } else {
            $string = "0" . $string;
        }

        if ($makeUnique) {
            // add time to string to make cypher text unique: add int time at the beginning and miliseconds
            // at the end so the entire encrypted string is unique (algorithm encrypts in blocks)
            // format: [header: 2 chars][int time: 4 chars][string][int microtime: 2 chars] = XXXXXXstringXX

            $string = pack("s", 8372) . pack("L", time()) . $string . pack("s", substr(microtime(true), -4));
        } else {
            $string = pack("s", 4772) . $string;
        }

        $string .= "+"; // make sure the last character is not a null, so it doesn't get trimmed

        return bin2hex(
            mcrypt_encrypt(
                "blowfish",
                $password,
                $string,
                MCRYPT_MODE_ECB,
                mcrypt_create_iv(8, MCRYPT_RAND) // 8 is iv length for blowfish
            )
        );
    }

    /**
     * Decrypts a string encrypted with Object::encrypt().
     *
     * @param string $string String to decrypt.
     *
     * @return string
     */
    public static function decrypt($string)
    {
        if (!$string || !function_exists("mcrypt_decrypt")) {
            return false;
        }

        $output = trim(
            mcrypt_decrypt(
                "blowfish",
                config("encryptionPassword"),
                @pack("H*", $string),
                MCRYPT_MODE_ECB,
                mcrypt_create_iv(8, MCRYPT_RAND)
            )
        );

        // check header

        $header = substr($output, 0, 2);

        if ($header == pack("s", 4772)) { // not unique
            $output = substr($output, 2, -1);
        } elseif ($header == pack("s", 8372)) { // unique
            $output =  substr($output, 6, -3); // -3 = miliseconds and the + sign
        } else {
            return false;
        }

        // check if it's numeric and unpack

        $numeric = substr($output, 0, 1);

        if ($numeric === "0") {
            return substr($output, 1);
        } elseif ($numeric === "1") {
            $array = unpack("L", substr($output, 1));
            return $array[1];
        }

        return false;
    }

    public static function encryptArray($array)
    {
        return self::encrypt(json_encode($array, JSON_HEX_TAG));
    }

    public static function decryptArray($string)
    {
        $decrypted = self::decrypt($string);

        if (!$decrypted) {
            return false;
        }

        return json_decode($decrypted);
    }

    static public function sizeToString($size)
    {
        if ($size / 1073741824 >= 1)
            return sprintf(tr("%s GB"), round($size / 1073741824));

        else if ($size / 1048576 >= 1)
            return sprintf(tr("%s MB"), round($size / 1048576));

        else if ($size / 1024 >= 1)
            return sprintf(tr("%s kB"), round($size / 1024));

        else if ($size < 1024 && $size > 0)
            return sprintf(ngettext("%s byte", "%s bytes", $size), $size);

        else
            return "0";
    }

    public static function structuredDirectory($id, $idsPerDir = 500, $levels = 2)
    {
        // returns the folder number for creating folder structure to store more files than can be held in one folder

        if ($idsPerDir <= 0) {
            $idsPerDir = 100;
        }

        if ($levels < 1 || $levels > 3) {
            $levels = 2;
        }

        $level1 = floor($id / $idsPerDir);
        $level2 = floor($level1 / 1000);
        $level3 = floor($level2 / 1000);

        return
        ($levels > 2 ? sprintf("%03d", $level3 % 1000) . "/" : "") .
        ($levels > 1 ? sprintf("%03d", $level2 % 1000) . "/" : "") .
        sprintf("%03d", $level1 % 1000) . "/";
    }

    public static function structuredFile($id, $number = 0, $role = false, $ext = "jpg", $idsPerDir = 500)
    {
        // creates a path for pictures in data directory
        // number and role allow for creating variations of pictures for the id, they can be any values

        return
            self::structuredDirectory($id, $idsPerDir) .
            self::encodeId($id, $number, $role) .
            "." . $ext;
    }

    public static function deleteOutdatedFiles($directory, $interval, &$error)
    {
        $directory = self::removeSlash($directory);

        if (!$handle = opendir($directory)) {
            $error = "Cannot open directory"; // not translated, logged for admins only
            return false;
        }

        while (($file = readdir($handle)) !== false) {
            if ($file != "." && $file != ".." && filemtime($directory . '/' . $file) < time() - $interval) {
                unlink($directory . '/' . $file);
            }
        }

        closedir($handle);
        return true;
    }

    public static function ensureFileName($string)
    {
        $ch = '\/:?*+%|"<>';
        $string = strtolower($string);
        $output = '';

        for ($i = 0; $i < strlen($string); $i++) {
            $output .= strpos($ch, $string[$i]) === false ? $string[$i] : "_";
        }

        return $output;
    }

    public static function uniqueFileName($path, $ext = false, $prefix = false)
    {
        if (strlen($ext) && $ext[0] != ".") {
            $ext = "." . $ext;
        }

        $path = self::addSlash($path);

        do {
            $fileName = $path . $prefix . uniqid() . $ext;
        } while (file_exists($fileName));

        return $fileName;
    }

    public static function ext($fileName)
    {
        return strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
    }

    public static function getFileName($originalName, $recordId, $number = 0, $role = 'orginal', $type = 'structured')
    {
        $ext = File::ext($originalName);

        switch ($type) {
            case "random":
                $fileName = mt_rand() . "." . $ext;
                break;
            case "encodedId":
                $fileName = File::encodeId($recordId) . "." . $ext;
                break;
            case "structured":
                $fileName = File::structuredFile($recordId, $number, $role, $ext, 40);
                break;
            default:
                $fileName = $originalName;
        }

        return $fileName;
    }

    /**
     * Converts array to a quoted string that is safe to pass to a js function.
     *
     * @param array $array
     */
    public static function jsParam($params)
    {
        is_array($params) || $params = [$params];

        foreach ($params as $key => $val) {
            $params[$key] = addslashes((string)$val);
        }

        return "'" . implode("','", $params) . "'";
    }

    /**
     * Encode an integer to the shortest string possible that can be used in a url.
     *
     * @param unknown $number
     * @param string $pad
     * @return string
     */
    public static function base36encode($number)
    {
        $codeset = "0123456789abcdefghijklmnopqrstuvwxyz";
        $base = strlen($codeset);
        $result = "";

        while ($number > 0) {
          $result = substr($codeset, ($number % $base), 1) . $result;
          $number = floor($number / $base);
        }

        return $result;
    }

    public static function base36decode($string)
    {
        $codeset = "0123456789abcdefghijklmnopqrstuvwxyz";
        $base = strlen($codeset);
        $len = strlen($string);
        $result = 0;

        for ($i = $len; $i; $i--) {
            $result += strpos($codeset, substr($string, (-1 * ($i - strlen($string) )), 1)) * pow($base, $i - 1);
        }

        return $result;
    }
}
