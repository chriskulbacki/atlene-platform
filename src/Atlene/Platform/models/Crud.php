<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Eloquent;
use Route;

abstract class Crud extends Eloquent
{
    protected $table = null;

    /**
     * Registers a resource route set for crud. This should be used instead of Route::resource() because
     * Route::resource() always uses the url prefix to create the route names and if the admin url is changed
     * by the developers from "admin" to something else (using overrides), the route names change as well,
     * while they need to be prefixed as admin. It doesn't matter what the url is, we always want the admin
     * routes to be prefixed with "admin".
     *
     * @param string $name
     * @param string $controller
     */
    public static function addRoute($name, $controller)
    {
        Route::resource(
            $name,
            $controller,
            [
                "names" => [
                    "index" => "admin.$name.index",
                    "create" => "admin.$name.create",
                    "store" => "admin.$name.store",
                    "edit" => "admin.$name.edit",
                    "update" => "admin.$name.update",
                    "destroy" => "admin.$name.destroy",
                ],
                "except" => ["show"],
            ]
        );
    }
}
