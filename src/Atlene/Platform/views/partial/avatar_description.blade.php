<p class='text-muted'>
    {!!
        sprintf(
            tr("Upload the avatar image that you'd like to use. If this field is left empty, the program will use the image from %s, if available."),
            "<a href='http://gravatar.com'>gravatar.com</a>"
        )
    !!}
</p>