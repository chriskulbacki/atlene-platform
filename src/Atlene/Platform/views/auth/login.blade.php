<div class='login-container login-box'>
    @if (Session::has("message"))
        <div class='login-error alert alert-success'>{{ Session::get("message") }}</div>
    @endif

    @if($errors->any())
        <div class='login-error alert alert-danger'>{{ $errors->first() }}</div>
    @endif

    {!! Form::open(["method" => "POST", "url" => $action]) !!}
        <div class='form-group email'>
            {!! Form::label("email-input", tr("E-mail")) !!}
            {!! Form::email("email", null, ["class" => "form-control", "id" => "email-input", "autofocus" => "autofocus"]) !!}
        </div>
        <div class='form-group password'>
            {!! Form::label("password-input", tr("Password")) !!}
            {!! Form::password("password", ["class" => "form-control", "id" => "password-input"]) !!}
        </div>
        <div class='form-group buttons'>
            <button type='submit' class='btn btn-primary submit'>{{ tr("Login") }}</button>
        </div>
        <div class='form-group options'>
            {!! Form::checkbox("remember", 1, true, ["id" => "remember"]) !!}
            {!! Form::label("remember", tr("Stay signed in")) !!}
            <a href='{{ $passwordReset }}' class='pull-right'>{{ tr("I forgot my password") }}</a>
        </div>
    {!! Form::close() !!}
</div>