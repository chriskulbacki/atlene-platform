<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

return [

    // black
    "000000" => [
        "navbar"        => ["000000", "2a2a2a", "000000", "eeeeee", "ffffff"],
    ],

    // gray
    "3f3f3f" => [
        //                  normal    primary   muted
        "text"          => ["333333", "04548b", "888888"],

        //                  bg norm   bg dark1  bg dark2  text      txt-high
        "navbar"        => ["3f3f3f", "333333", "0f0f0f", "eeeeee", "ffffff"],
        "sidebar"       => ["3f3f3f", "333333", "0f0f0f", "eeeeee", "ffffff"],
        "table"         => ["3f3f3f", "333333", "0f0f0f", "eeeeee", "ffffff"],
        "btn-default"   => ["ffffff", "eeeeee", "ffffff", "333333", "000000"],
        "btn-primary"   => ["0470bb", "0461a2", "04548b", "eeeeee", "ffffff"],
        "btn-success"   => ["469b47", "3d8d3f", "347d35", "eeeeee", "ffffff"],
        "btn-info"      => ["45a9d7", "3d9cc9", "358eb8", "eeeeee", "ffffff"],
        "btn-warning"   => ["ec971f", "da8a1a", "ca7f15", "eeeeee", "ffffff"],
        "btn-danger"    => ["db3530", "cd2b26", "bd2521", "eeeeee", "ffffff"],

        //                  bg-1      bg-2      border    text
        "alert-success" => ["eeffe7", "cdedbf", "9bc789", "367937"],
        "alert-info"    => ["e8f7ff", "bedceb", "8ec2dc", "29617d"],
        "alert-warning" => ["fffcea", "e9e1b7", "dbb471", "7e6231"],
        "alert-danger"  => ["ffeaea", "f2d0d0", "dca7a7", "af3634"],
    ],

    // blue
    "0470bb" => [
        "navbar"        => ["0470bb", "0461a2", "04548b", "eeeeee", "ffffff"],

        "btn-primary"   => ["a66504", "7f4c01", "6E4100", "eeeeee", "ffffff"],
        "btn-success"   => ["367937", "2b702c", "1a5a1b", "eeeeee", "ffffff"],

    ],

    // light blue
    "309ae3" => [
        "navbar"        => ["309ae3", "2385c8", "176fac", "eeeeee", "ffffff"],

        "btn-primary"   => ["b77109", "a36405", "8c5503", "eeeeee", "ffffff"],
        "btn-success"   => ["50ab51", "429743", "368537", "eeeeee", "ffffff"],
        "btn-info"      => ["d484e5", "c36ed5", "b562c7", "eeeeee", "ffffff"],
        "btn-danger"    => ["e94a45", "d23c38", "bf302c", "eeeeee", "ffffff"],
    ],

    // green
    "009928" => [
        "navbar"        => ["009928", "017f22", "006d1c", "eeeeee", "ffffff"],
        "btn-success"   => ["ad572d", "964923", "813b19", "eeeeee", "ffffff"],
    ],

    // light green
    "14b93f" => [
        "navbar"        => ["14b93f", "0d9d32", "088729", "eeeeee", "ffffff"],

        "btn-primary"   => ["309ae3", "268bd0", "1c73af", "eeeeee", "ffffff"],
        "btn-success"   => ["c16d43", "ad5e38", "954e2b", "eeeeee", "ffffff"],
        "btn-danger"    => ["e94a45", "d23c38", "bf302c", "eeeeee", "ffffff"],
    ],

    // brown
    "7b5003" => [
        "navbar"        => ["7b5003", "684302", "553702", "eeeeee", "ffffff"],
    ],

    // light brown
    "b07309" => [
        "navbar"        => ["b97c11", "a36c0d", "8e5e0a", "eeeeee", "ffffff"],
    ],

    // red
    "b10f0f" => [
        "navbar"        => ["b10f0f", "990808", "850909", "eeeeee", "ffffff"],
    ],

    // light red
    "e04040" => [
        "navbar"        => ["e04040", "cb3636", "b52d2d", "eeeeee", "ffffff"],
    ],

    // purple
    "9d19b9" => [
        "navbar"        => ["9d19b9", "8913a2", "7a0f91", "eeeeee", "ffffff"],
    ],

    // light purple
    "ca5ee1" => [
        "navbar"        => ["ca5ee1", "b84bcf", "a63cbc", "eeeeee", "ffffff"],
    ],
];