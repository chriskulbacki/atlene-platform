<div class='password-get-email-container login-box'>
    @if (Session::has("success"))
        <div class='login-error alert alert-success'>{{ Session::get("success") }}</div>
        <div class='form-group buttons'>
            <a href='{{ $loginRoute }}' class='btn btn-primary'>{{ tr("Go to login page") }}</a>
        </div>
    @else

        @if (Session::has("error"))
            <div class='login-error alert alert-danger'>{{ Session::get("error") }}</div>
        @endif

        {!! Form::open(["autocomplete" => "off"]) !!}
            <p>{{ tr("Instructions to reset your password will be send to the email you used when creating your account.") }}</p>
            <div class='form-group'>
                {!! Form::label("email-input", tr("E-mail")) !!}
                {!! Form::email("email", null, ["class" => "form-control", "id" => "email-input", "autofocus" => "autofocus"]) !!}
            </div>

            <div class='form-group buttons'>
                <button type='submit' class='btn btn-primary submit'>{{ tr("Send") }}</button>
                <a href='{{ $loginRoute }}' class='btn btn-success'>{{ tr("Back to login") }}</a>
            </div>
        {!! Form::close() !!}
    @endif
</div>