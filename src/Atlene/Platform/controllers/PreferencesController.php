<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Http\Request;
use Auth;
use Response;

class PreferencesController extends AccountController
{
    protected $modelName = "Atlene\Platform\Preferences";

    public function getTabs()
    {
        return [
            "user" => tr("User"),
            "avatar" => tr("Avatar"),
            "password" => tr("Password"),
            "preferences" => tr("Preferences"),
        ];
    }

    /**
     * (non-PHPdoc)
     * @see \Atlene\Platform\AccountController::getFields()
     */
    public function getFields()
    {
        // We don't just unset the values we don't want because some values can be added to AccountController
        // later and if we forget to unset them here we'll have a problem. So we select the values we want.

        $old = parent::getFields();
        $new = [];

        $new['first_name'] = $old['first_name'];
        $new['last_name'] = $old['last_name'];
        $new['email'] = $old['email'];
        $new['sex'] = $old['sex'];
        $new['birth_date'] = $old['birth_date'];
        $new['avatar'] = $old['avatar'];
        $new['avatar_description'] = $old['avatar_description'];
        $new['password'] = $old['password'];
        $new['password_confirmation'] = $old['password_confirmation'];
        $new['date_format'] = $old['date_format'];
        $new['date_format_links'] = $old['date_format_links'];
        $new['time_format'] = $old['time_format'];
        $new['time_format_links'] = $old['time_format_links'];
        $new['date_format_description'] = $old['date_format_description'];
        $new['time_format_description'] = $old['time_format_description'];

        $new['password']['tab'] = "password";
        $new['password_confirmation']['tab'] = "password";

        return $new;
    }

    public function edit($id)
    {
        Backend::setPageTitle(tr("Personal preferences"));
        Backend::setBreadcrumbs([tr("Account") => false, tr("Preferences") => false]);

        return parent::edit($id);
    }

    public function saveColor(Request $request)
    {
        try {
            if (!Auth::check()) {
                throw new \Exception();
            }

            if (!$request->has("backendColor") && !$request->has("backendGradient")) {
                throw new \Exception;
            }

            $user = User::find(Auth::id());

            if ($request->has("backendColor")) {
                $user->backend_color = $request->input("backendColor");
            }

            if ($request->has("backendGradient")) {
                $user->backend_gradient = $request->input("backendGradient");
            }

            if (!$user->save()) {
                throw new \Exception();
            }

            return Response::json([ "status" => "ok" ]);

        } catch (\Exception $e) {
            return Response::json(
                [
                    "status" => "error",
                    "message" => tr("Cannot save the color settings."),
                ]
            );
        }
    }

}