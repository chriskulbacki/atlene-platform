<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use App\User;
use Validator;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Set this to false to disable login throttling.
     */
    protected $throttle = true;

    /**
     * Specify how many login attempts are permitted.
     */
    protected $maxLoginAttempts = 5;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:' . Setting::get("table_users"),
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function loginPath()
    {
        return Site::route("login");
    }

    protected function getLockoutErrorMessage($seconds)
    {
        return sprintf(tr("Too many login attempts. Please try again in %s seconds."), $seconds);
    }

    /**
     * Handle displaying the login page.
     *
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        // using this instead of guest middleware
        if (Auth::check()) {
            return redirect()->to(Site::route("admin.dashboard"));
        }

        return $this->view(
            override("platform::auth.login"),
            [
                "title" => tr("Login"),
                "action" => Site::route("login"),
                "passwordReset" => Site::route("password.email"),
            ]
        );
    }

    /**
     * Handle the user login action.
     *
     * @param Request $request
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        if ($this->throttle && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        try {
            $credentials = $request->only($this->loginUsername(), "password");

            if (empty($credentials[$this->loginUsername()]) || empty($credentials['password'])) {
                throw new \Exception(tr("Please type email and password."));
            }

            if (!Auth::attempt($credentials, $request->has("remember"))) {
                $this->incrementLoginAttempts($request);
                sleep(4);
                throw new \Exception(tr("Invalid email or password"));
            }

            if (!Auth::user()->activated_at) {
                Auth::logout();
                throw new \Exception(tr("This account is not activated."));
            }

            if (Auth::user()->suspended_at) {
                Auth::logout();
                throw new \Exception(tr("This account has been suspended."));
            }

            // remember last login in case we want to display it to the user

            session(["last_login" => Auth::user()->last_login]);

            // save login time
            Auth::user()->last_login = date("Y:m:d H:i:s");
            Auth::user()->save();

            // clear the invalid login attempts log

            if ($this->throttle) {
                $this->clearLoginAttempts($request);
            }

            // redirect to the intended route or dashboard
            return redirect()->intended(Site::route("admin.dashboard"));

        } catch (\Exception $e) {
            return redirect()->to(Site::route("login"))->withErrors($e->getMessage());
        }
    }

    /**
     * Logs out the user. Using post instead of get to prevent browser page pre-fetching which might automatically
     * perform the logout.
     */
    public function getLogout()
    {
        Auth::logout();

        return $this->view(
            override("platform::auth.logout"),
            [
                "title" => tr("Logout"),
                "loginRoute" => Site::route("login"),
                "siteRoute" => Site::url(),
            ]
        );
    }
}
