<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Config;
use App;

abstract class Configuration
{
    protected $name = null;
    protected $config = null;
    protected $id = null;

    public function __construct()
    {
        $this->loadConfig();
        $this->setCurrent();
    }

    public function getId()
    {
        return $this->id;
    }

    public function get($key = false)
    {
        $array = $this->id ? $this->config[$this->id] : $this->config;

        if (!$key) {
            return $array;
        }

        return array_key_exists($key, $array) ? $array[$key] : false;
    }

    public function getItem($id)
    {
        return array_key_exists($id, $this->config) ? $this->config[$id] : [];
    }

    public function getAll()
    {
        return $this->config;
    }

    public function getCount()
    {
        return count($this->config);
    }

    public function loadConfig()
    {
        if (!$this->name) {
            throw new \Exception("Configuration name not specified.");
        }

        $appConfig = config_path("{$this->name}.php");

        if (file_exists($appConfig)) {
            $this->config = include($appConfig);
        } else {
            $this->config = include(__DIR__ . "/../config/{$this->name}.php");
        }

        $this->fixConfig();
    }

    public function fixConfig()
    {
    }

    protected function removeNonNumericKeys()
    {
        foreach ($this->config as $id => $item) {
            if (!is_numeric($id)) {
                unset($this->config[$id]);
            }
        }

        !empty($this->config) || $this->config = [1 => []];
    }

    public function setCurrent()
    {

    }

    protected function setIdByKey($key, $value)
    {
        foreach ($this->config as $id => $item) {
            if ($item[$key] == $value) {
                $this->id = $id;
                break;
            }
        }

        // if not found, set it to the first id
        $this->id || $this->id = 1;
    }
}
