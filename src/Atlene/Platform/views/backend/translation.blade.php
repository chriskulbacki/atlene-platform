<h4>{{ tr("Generate App POT file") }}</h4>
<p>
    {{ tr("Click the button below to extract translation strings from the source code of your application and create a new gettext POT file.") }}
    {{ tr("Please don't refresh your browser when the file is being generated.") }}
</p>
<p class='buttons'>
    <a href='{{ $downloadPotUrl }}' class='btn btn-default'>{{ tr("Download POT file") }}</a>
</p>
<p>
    {{ tr("The POT template can be used to create a localized PO file, which can be translated and compiled to the binary MO file that will be used by the application.") }}
    {{ tr("All this process can be accomplished using the Poedit application.") }}
</p>
<p class='buttons'>
    <a href='https://poedit.net' target="_blank" class='btn btn-default'>{{ tr("Download Poedit") }}</a>
</p>
<p>
    {{ sprintf(tr("The translated and compiled MO file should be placed in the folder %s, following the gettext folder structure."), "app/Translations") }}
</p>
<p>
    {{ tr("For example, for Spanish, the path to the compiled translation file should be:") }}
</p>
<p>
    <pre>app/Translations/es_ES/LC_MESSAGES/app.mo</pre>
</p>
