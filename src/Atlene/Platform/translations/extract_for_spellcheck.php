<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */


/**
 * Extracts all the English strings from the pot file into a single file that can then be opened in an editor and spell-checked.
 */

$string = file_get_contents(__DIR__ . "/messages.pot");
$i = 0;
$count = 0;
$output = "";

while ($i = strpos($string, "msgid", $i + 1)) {
    if ($j = strpos($string, "msgstr", $i + 1)) {
        $s = substr($string, $i + 6, $j - $i - 6);
        $s = trim(strip_tags(str_replace(array('"', "\n"), "", $s)));
        $s = str_replace("%s", " ", $s);
        $s = str_replace("%d", " ", $s);
        $s = str_replace("msgid", " ", $s);
        $s = str_replace("_", " ", $s);
        $s = str_replace('%1$s', " ", $s);
        $s = str_replace('%2$s', " ", $s);
        $s = str_replace('%3$s', " ", $s);
        $s = str_replace('%4$s', " ", $s);
        $s = str_replace('%1$d', " ", $s);
        $s = str_replace('%2$d', " ", $s);
        $s = str_replace('%3$d', " ", $s);
        $s = str_replace('%4$d', " ", $s);

        $output .= " " . $s;
    }
}

file_put_contents(__DIR__ . "/spellcheck.txt", $output);

echo "Done.\n\n";
