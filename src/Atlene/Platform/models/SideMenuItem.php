<?php namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class SideMenuItem extends MenuItem implements IMenuItem
{
    public static function make($title, $route, $routeParameters = []) {
        return new self($title, $route, $routeParameters);
    }

    public static function makeCrud($title, $createTitle, $editTitle, $route) {

        $item = self::make($title, "$route.index");

        $item->add(
            self::make($createTitle, "$route.create")->setHidden(true)
        )->add(
            self::make($editTitle, "$route.edit")->setHidden(true)
        );

        return $item;
    }

    public function render($markActive = false)
    {
        $routeId = crc32($this->route);

        // get subitems, if any
        $sub = $this->renderItems();

        // if it's an item (doesn't have subitems), check if its route is allowed
        if (!$sub && !Access::isRouteAllowed($this->getRoute())) {
             return false;
        }

        // if the item contains html code, return the code
        if (!$sub && $html = $this->getHtml()) {
            return $html;
        }

        // create the link element and the subitems

        $title = Element::span($this->title);

        if ($this->icon) {
            $title = Element::i(false, array("class" => "fa fa-" . $this->icon)) . " " . $title;
        }

        if ($sub = $this->renderItems()) {
            $a = Element::a(
                "javascript:void(0)",
                $title,
                ["onclick" => "sidebar.toggleItem(this, $routeId)"]
            );
            $div = Element::div($sub, ["class" => "sub list-group"]);
        } else {
            $a = Element::a($this->getUrl(), $title);
            $div = false;
        }

        // don't render empty groups
        if ($a->attr("href") == "javascript:void(0)" && empty($div)) {
            return false;
        }

        if ($this->hasTarget()) {
            $a->addClass("target");
        }

        $a->addClass("sidebar-$routeId list-group-item");

        // wrap the link in a list item and return it

        if ($sub) {
            $a->addClass("dropdown");
        }

        if ($this->hidden) {
            $a->addClass("hidden");
        }

        if ($this->rootItem) {
            $a->addClass("root-item");
        }

        if ($this->rootItem && ($this->active || $markActive)) {
            $a->addClass("active");
        }

        if (isset($_COOKIE["open-item-$routeId"]) || $this->active || $markActive) {
            $a->addClass("open");
            if ($div) {
                $div->addClass("open");
            }
        }

        return (string)Element::div($a . $div, ["class" => "wrap"]);
    }



}
