<table class='table table-condensed'>
    <tr>
        <th colspan='2'>{{ tr("Hour") }}</th>
        <th colspan='2'>{{ tr("Minute") }}</th>
        <th colspan='2'>{{ tr("Second") }}</th>
    </tr>
    <tr>
        <td>H</td>
        <td>0 1 ... 22 23</td>
        <td>mm</td>
        <td>00 01 ... 58 59</td>
        <td>ss</td>
        <td>00 01 ... 58 59</td>
    </tr>
    <tr>
        <td>HH</td>
        <td>00 01 ... 22 23</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>h</td>
        <td>1 2 ... 11 12</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>hh</td>
        <td>01 02 ... 11 12</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>A</td>
        <td>AM PM</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>a</td>
        <td>am pm</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>