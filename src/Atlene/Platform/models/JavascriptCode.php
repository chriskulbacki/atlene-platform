<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class JavascriptCode
{
    protected $locales = false;

    public function __construct()
    {
        $this->setLibraryLocales();
    }

    /**
     * Sets the locales of the listed javascript libraries we're using according to the currently loaded site locale.
     *
     * @return multitype:multitype:boolean multitype:string
     */
    protected function setLibraryLocales()
    {
        // this array needs to be inside the function because we're using override()--this enables devs to specify
        // their own paths for loading the translation files from in case they use their own

        $this->locales = [
            "moment" => [
                "path" => override("bower_components/moment/locale/%s.js"),
                "locales" => [
                    "af", "ar", "ar-ma", "ar-sa", "ar-tn", "az", "be", "bg", "bn", "bo", "br", "bs", "ca", "cs", "cv",
                    "cy", "da", "de-at", "de", "el", "en-au", "en-ca", "en-gb", "eo", "es", "et", "eu", "fa", "fi",
                    "fo", "fr-ca", "fr", "fy", "gl", "he", "hi", "hr", "hu", "hy-am", "id", "is", "it", "ja", "ka",
                    "km", "ko", "lb", "lt", "lv", "mk", "ml", "mr", "ms-my", "my", "nb", "ne", "nl", "nn", "pl",
                    "pt-br", "pt", "ro", "ru", "sk", "sl", "sq", "sr-cyrl", "sr", "sv", "ta", "th", "tl-ph", "tr",
                    "tzm", "tzm-latn", "uk", "uz", "vi", "zh-cn", "zh-tw",
                ],
                "value" => false,
            ],

            "bootstrap-fileinput" => [
                "path" => override("bower_components/bootstrap-fileinput/js/fileinput_locale_%s.js"),
                "locales" => [
                    "bg", "cr", "cz", "de", "el", "es", "fa", "fr", "hu", "it", "nl", "pl", "pt-BR", "pt", "ro", "ru",
                    "sk", "th", "tr", "uk", "zh", "zh-TW",
                ],
                "value" => false,
            ],
        ];

        $code = strtolower(Locale::get("code"));
        $language = strtolower(str_replace("_", "-", Locale::get("language")));

        foreach ($this->locales as $library => $data) {
            // try language first (es_ES)
            if ($key = array_search($language, array_map('strtolower', $data['locales']))) {
                $this->locales[$library]['value'] = $data['locales'][$key];
                continue;
            }

            // try code (es)
            if ($key = array_search($code, array_map('strtolower', $data['locales']))) {
                $this->locales[$library]['value'] = $data['locales'][$key];
                continue;
            }
        }
    }

    /**
     * Get the array of paths leading to the javascript library locale files we need to load in the header.
     */
    public function getLibraryLocalePaths()
    {
        $result = [];

        foreach ($this->locales as $library => $data) {
            if ($data['value']) {
                $result[] = sprintf($data['path'], $data['value']);
            }
        }

        return $result;
    }

    /**
     * Returns the array of available locales for the specified library.
     *
     * @param string $library
     */
    public function getLibraryAvailableLocales($library)
    {
        return $this->locales[$library]['locales'];
    }

    /**
     * Returns the current locale value of the specified library. This can be used in case the library needs to be
     * initialized with a language value and it's not enough to just load the language file.
     *
     * @param unknown $library
     */
    public function getLibraryLocale($library)
    {
        return $this->locales[$library]['value'];
    }
}