<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

/**
 * Counts characters to be translated in messages.pot.
 */

$string = file_get_contents(__DIR__ . "/platform.pot");
$i = 0;
$count = 0;

while ($i = strpos($string, "msgid", $i + 1)) {
    if ($j = strpos($string, "msgstr", $i + 1)) {
        $s = substr($string, $i + 6, $j - $i - 6);
        $s = trim(strip_tags(str_replace(array('"', "\n"), "", $s)));
        $count += strlen($s);
    }
}

echo "Character count: $count\n".
    "Page count: " . round($count / 1800) . "\n\n";
