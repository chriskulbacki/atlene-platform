<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class TopMenuItem extends MenuItem implements IMenuItem
{
    public static function make($title, $route) {
        return new self($title, $route);
    }

    public function render()
    {
            // get subitems, if any
        $sub = $this->renderItems();

        // if it's an item (doesn't have subitems), check if its route is allowed
        if (!$sub && !Access::isRouteAllowed($this->getRoute())) {
             return false;
        }

        // if the item contains html code, return the code
        if (!$sub && $html = $this->getHtml()) {
            return $html;
        }

        if ($sub) {
            $output =
                Element::li(
                    Element::a(
                        false,
                        Element::span(
                            $this->title . " " .
                            Element::i(false, array("class" => "fa fa-caret-down")),
                            array(),
                            true
                        ),
                        array("class" => "dropdown-toggle", "data-toggle" => "dropdown")
                    ).
                    Element::ul($sub)->addClass("dropdown-menu")
                )->addClass("dropdown")->addClass($this->class);

        } else {
            if ($this->icon) {
                $title = Element::i(false, array("class" => "fa fa-" . $this->icon)) . " " .
                    Element::span($this->title);
            } else {
                $title = $this->title;
            }
            $output = Element::li(Element::a($this->getUrl(), $title));
        }

        return (string)$output;
    }
}
