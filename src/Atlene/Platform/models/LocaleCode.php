<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Session;
use Input;

class LocaleCode extends Configuration
{
    protected $name = "atlene.locales";
    public static $localeFromUrl = false;
    private $domains = [];
    private $jsTranslations = [];

    /**
     * Set site language according to the url (for example: http://site.com/en). After the language is set
     * the language part is removed from $_SERVER['REQUEST_URI'] - from then on Laravel doesn't see the language
     * part when processing all its routes. That's why this function must be called before Laravel starts up.
     *
     * Set the static $requestLocale variable so we can set the language in loadConfig().
     */
    public static function setLocaleFromUrl()
    {
        if (!array_key_exists("REQUEST_URI", $_SERVER)) {
            return;
        }

        $uri = $_SERVER['REQUEST_URI'];
        $parts = explode("/", $uri);

        if (!empty($parts[1])) {
            $locales = include(__DIR__ ."/../resources/locales/locales_en.php");

            foreach ($locales as $item) {
                if (strcasecmp($item[1], $parts[1]) == 0) {
                    self::$localeFromUrl = $parts[1];
                    $_SERVER['REQUEST_URI'] = substr($uri, strlen($parts[1]) + 1);
                    return;
                }
            }
        }
    }

    public function addGettextDomain($name, $path)
    {
        $this->domains[$name] = $path;
    }

    public function loadGettext()
    {
        // check if gettext is installed and available
        if (!defined('LC_MESSAGES')) {
            return false;
        }

        // set locale, don't set LC_ALL because we don't want to set LC_NUMERIC - it'll cause problems when
        // it starts treating periods as commas and vice-versa in floating points
        $language = $this->config[$this->id]['language'] . ".UTF-8";
        $categories = [
            LC_COLLATE => "LC_COLLATE",
            LC_CTYPE => "LC_CTYPE",
            LC_TIME => "LC_TIME",
            LC_MESSAGES => "LC_MESSAGES",
            LC_MONETARY => "LC_MONETARY",
        ];

        foreach ($categories as $key => $val) {
            putenv("$val=$language");
            setlocale($key, $language);
        }

        // bind the registered domains
        foreach ($this->domains as $name => $path) {
            bindtextdomain($name, $path);
            bind_textdomain_codeset($name, "UTF-8");
        }

        // set the current domain to app
        textdomain("app");
    }

    public function setCurrent()
    {
        parent::setCurrent();
        $code = false;

        try {
            // set locale from the url (http://site.com/de)
            if (self::$localeFromUrl) {
                $code = self::$localeFromUrl;
                throw new \Exception();
            }

            // set locale from url parameter: ?locale=de (backend only)
            if (Site::isBackend() && $code = Input::get("locale")) {
                \Session::put("locale-code", $code);
                Q::metaRedirect(Q::removeVarsFromUrl("locale"));
                throw new \Exception();
            }

            // set locale from the session (backend only)
            if (Site::isBackend() && $code = \Session::get("locale-code")) {
                $this->setLocaleId($code);
                throw new \Exception();
            }

            // if locale not set in url, set it to the default site locale
            // not using the facade because we'd then need to re-load its config after the locale is set
            // to get its translated strings to render properly. This way we use a temporary object
            // and the facade loads its config on first access with already translated strings.

            $code = Site::get("locale_code");

        } catch (\Exception $e) {
        }

        parent::setIdByKey("code", $code);
    }

    public function fixConfig()
    {
        $this->removeNonNumericKeys();

        foreach ($this->config as $key => $options) {
            array_key_exists("code", $options) || $this->config[$key]['code'] = "en";
            array_key_exists("language", $options) || $this->config[$key]['language'] = "en_US";
            array_key_exists("name", $options) || $this->config[$key]['name'] = "English";
            array_key_exists("translated_name", $options) || $this->config[$key]['translated_name'] = "English";
            array_key_exists("currency_format", $options) || $this->config[$key]['currency_format'] = "$#,###.##";
        }
    }

    public function addJsTranslation($key, $value)
    {
        $value = str_replace("'", "\'", $value);
        $this->jsTranslations[$key] = "$key:'$value'";
    }

    public function getJsTranslations()
    {
        return "tr = {" . implode(",", $this->jsTranslations) . "};";
    }
}
