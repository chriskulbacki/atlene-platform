<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reset password</title>
    <style>
        body {
            background: #eee;
            padding: 20px;
            color: #333;
            font-size: 80%;
        }
        #page {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background: #fff;
            border: 1px solid #ddd;
        }
        h3 {
            font-size: 1.4em;
            margin: 20px 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <?php $url = URL::to('password/reset', array($token)); ?>

    <div id='page'>
        <h3>Reset password</h3>
        <p>
            To reset your password, complete this form:
        </p>

        <p>
            <a href='{{ $url }}'>{{ $url }}</a>
        </p>

        <p>
            This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes. If you have any questions don't hesitate to contact us.
        </p>
    </div>
</body>
</html>

