<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */


use Illuminate\Support\ServiceProvider;
use Config;
use DB;
use Validator;
use Form;
use Request;

class PlatformServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // stop "composer update" from generating "class not found" errors
        if (!class_exists("\Atlene\Platform\Locale")) {
            return;
        }

        // override the authentication settings so we can use our own model and

        Config::set("auth.model", \Atlene\Platform\User::class);
        Config::set("auth.password.email", "platform::auth.password_send_email");

        // set up facades

        $this->app->bind("sitecode", function () { return new SiteCode; });
        $this->app->bind("localecode", function () { return new LocaleCode; });
        $this->app->bind("currencycode", function () { return new CurrencyCode; });
        $this->app->bind("settingcode", function () { return new SettingCode; });
        $this->app->bind("overridecode", function () { return new OverrideCode; });
        $this->app->bind("accesscode", function () { return new AccessCode; });
        $this->app->bind("backendcode", function () { return new BackendCode; });
        $this->app->bind("menucode", function () { return new MenuCode; });
        $this->app->bind("resourcecode", function () { return new ResourceCode; });
        $this->app->bind("colorcode", function () { return new ColorCode; });
        $this->app->bind("javascriptcode", function () { return new JavascriptCode; });

        // include functions

        require __DIR__ . "/functions.php";
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        // stop "composer update" from generating "class not found" errors
        if (!class_exists("\Atlene\Platform\Locale")) {
            return;
        }

        // inform the system where the views are

        $this->loadViewsFrom(__DIR__ . "/views", "platform");

        // set publishing paths (use "php artisan vendor:publish [--force]" to publish)

        $this->publishes([__DIR__ . "/config" => config_path()], "config");
        $this->publishes([__DIR__ . "/assets" => public_path("atlene/platform")], "public");
        $this->publishes([__DIR__ . '/migrations' => database_path('migrations')], "migrations");

        // add gettext domains for platform [use tr()] and application [use _()]

        Locale::addGettextDomain("platform", realpath(__DIR__ . "/translations/"));
        Locale::addGettextDomain("app", Setting::get("translation_target_dir"));

        // set up the current locale and bind gettext domains

        Locale::loadGettext();

        // reload the configs that were loaded in Locale::loadGettext() to get their proper translations

        Locale::loadConfig();
        Site::loadConfig();
        Setting::loadConfig();

        // query log uses a lot of memory, disable it unless we're running under a dev domain

        if (!Setting::isDevDomain()) {
            DB::disableQueryLog();
        }

        // include routes

        require __DIR__ . "/routes.php";

        // define additional validators

        require __DIR__ . "/validators.php";

        // register default backend permissions

        Access::addRoute(
            tr("General"),
            "admin.dashboard",
            tr("Access dashboard"),
            tr("Allows access to the admin dashboard.")
        );

        Access::addRoute(
            tr("User preferences"),
            "admin.preferences.*",
            tr("Edit user preferences"),
            tr("Allows access to the user preferences.")
        );

        Access::addRoute(
            tr("Accounts"),
            "admin.groups.*",
            tr("Groups"),
            tr("Allows adding, creating and deleting groups.")
        );

        Access::addRoute(
            tr("Accounts"),
            "admin.users.*",
            tr("Users"),
            tr("Allows adding, creating and deleting user accounts.")
        );

        // example of how to register crud permissions, this will add 4 permissions per item
        // instead of doing this, we register route permissions for accounts and groups below that adds only
        // one permission per item
        // Access::addCrud(tr("Groups"), "admin.groups", tr("Groups"));
        // Access::addCrud(tr("Users"), "admin.users", tr("Users"));

        // add default menu items

        Menu::addSideMenuItem(
            SideMenuItem::make(tr("Dashboard"), "admin.dashboard")->setIcon("home")->setWeight(0)
        );

         Menu::addSideMenuItem(
             SideMenuItem::make(tr("Accounts"), "accounts")->setIcon("user")->setWeight(998)
                ->add(SideMenuItem::makeCrud(tr("Users"), tr("Create user"), tr("Edit user"), "admin.users"))
                ->add(SideMenuItem::makeCrud(tr("Groups"), tr("Create group"), tr("Edit group"), "admin.groups"))
         );

         Menu::addSideMenuItem(
             SideMenuItem::make(tr("System"), "system")->setIcon("gear")->setWeight(999)
                ->add(SideMenuItem::make(tr("Translations"), "admin.translation"))
         );

//         example of adding custom html code to the sidebar menu:
//         Menu::addSideMenuItem(
//             SideMenuItem::make(tr("Code"), "code")->setIcon("home")
//                 ->add(SideMenuItem::make(tr("Html"), "admin.groups.*")->setHtml("hello"))
//         );
    }


}
