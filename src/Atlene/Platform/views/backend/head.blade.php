<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script>
    var csrfToken='{{ csrf_token() }}';
    var ksaUrl = '{{ route("atlene.ksa") }}';
</script>

<script src="{{ asset('atlene/platform/bower_components/jquery/dist/jquery.min.js') }}"></script>
<link href="{{ asset('atlene/platform/bower_components/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />

<script src="{{ asset('atlene/platform/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<link href="{{ asset('atlene/platform/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />

<script src="{{ asset('atlene/platform/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
<link href="{{ asset('atlene/platform/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}" rel="stylesheet" />

<script src="{{ asset('atlene/platform/bower_components/moment/min/moment.min.js') }}"></script>

<script src="{{ asset('atlene/platform/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<link href="{{ asset('atlene/platform/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />

<script src="{{ asset('atlene/platform/bower_components/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<link href="{{ asset('atlene/platform/bower_components/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet" />

<script src="{{ asset('atlene/platform/bower_components/js-cookie/src/js.cookie.js') }}"></script>
<script src="{{ asset('atlene/platform/bower_components/jquery-form/jquery.form.js') }}"></script>

@if (!empty($libraryLocales))
    @foreach ($libraryLocales as $item)
        <script src='{{ asset("atlene/platform/$item") }}'></script>
    @endforeach
@endif

<script src="{{ assetStamp('atlene/platform/backend/backend.js') }}"></script>
<link href="{{ assetStamp('atlene/platform/backend/backend.css') }}" rel="stylesheet" />
<link href="{{ assetStamp('atlene/platform/backend/colors.css') }}" rel="stylesheet" />

@if (!empty($jsTranslations))
<script>{{ $jsTranslations }}</script>
@endif

@if (!empty($head))
{!! $head !!}
@endif
