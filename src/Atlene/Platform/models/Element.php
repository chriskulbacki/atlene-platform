<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class Element
{
    private $type = "div";
    private $text = "";
    private $data = array();
    private $htmlText = false;
    private $rawText = false;

    public function __construct($type = "div", $text = "", array $attributes = array(), $rawText = false)
    {
        $this->type = $type;
        $this->text = $text;
        $this->htmlText = strpos(trim($text), "<") === 0;
        $this->data = array_merge($this->data, $attributes);
        $this->rawText = $rawText;
    }

    public function __toString()
    {
        return $this->output();
    }

    public function __set($attribute, $value)
    {
        $this->set($attribute, $value);
    }

    public function __get($attribute)
    {
        return $this->get($attribute);
    }

    public function __isset($attribute)
    {
        return $this->exists($attribute);
    }

    public function __unset($attribute)
    {
        $this->remove($attribute);
    }

    public function setType($type)
    {
        if ($type) {
            $this->type = $type;
        }

        return $this;
    }

    public function type()
    {
        return $this->type;
    }

    public function setText($text, $htmlText = false)
    {
        if ($text) {
            $this->text = $text;
            $this->htmlText = $htmlText;
        }

        return $this;
    }

    public function text()
    {
        return $this->text;
    }

    public static function div($text = false, array $attr = array(), $rawText = false)
    {
        return new self("div", $text, $attr, $rawText);
    }

    public static function span($text = false, array $attr = array(), $rawText = false)
    {
        return new self("span", $text, $attr, $rawText);
    }

    public static function label($text = false, array $attr = array(), $rawText = false)
    {
        return new self("label", $text, $attr, $rawText);
    }

    public static function i($text = false, array $attr = array(), $rawText = false)
    {
        return new self("i", $text, $attr, $rawText);
    }

    public static function b($text = false, array $attr = array(), $rawText = false)
    {
        return new self("b", $text, $attr, $rawText);
    }

    public static function p($text = false, array $attr = array(), $rawText = false)
    {
        return new self("p", $text, $attr, $rawText);
    }

    public static function h1($text = false, array $attr = array(), $rawText = false)
    {
        return new self("h1", $text, $attr, $rawText);
    }

    public static function h2($text = false, array $attr = array(), $rawText = false)
    {
        return new self("h2", $text, $attr);
    }

    public static function h3($text = false, array $attr = array(), $rawText = false)
    {
        return new self("h3", $text, $attr, $rawText);
    }

    public static function h4($text = false, array $attr = array(), $rawText = false)
    {
        return new self("h4", $text, $attr, $rawText);
    }

    public static function h5($text = false, array $attr = array(), $rawText = false)
    {
        return new self("h5", $text, $attr, $rawText);
    }

    public static function table($text = false, array $attr = array(), $rawText = false)
    {
        return new self("table", $text, $attr, $rawText);
    }

    public static function tr($text = false, array $attr = array(), $rawText = false)
    {
        return new self("tr", $text, $attr, $rawText);
    }

    public static function th($text = false, array $attr = array(), $rawText = false)
    {
        return new self("th", $text, $attr, $rawText);
    }

    public static function td($text = false, array $attr = array(), $rawText = false)
    {
        return new self("td", $text, $attr, $rawText);
    }

    public static function ol($text = false, array $attr = array(), $rawText = false)
    {
        return new self("ol", $text, $attr, $rawText);
    }

    public static function ul($text = false, array $attr = array(), $rawText = false)
    {
        return new self("ul", $text, $attr, $rawText);
    }

    public static function li($text = false, array $attr = array(), $rawText = false)
    {
        return new self("li", $text, $attr, $rawText);
    }

    public static function br()
    {
        return new self("br");
    }

    public static function input($type, $name, $value = "", $attr = array())
    {
        $element = new self("input", false, array_merge(array("type" => $type), $attr));
        $name && $element->setAttr("name", $name);
        $value && $element->setAttr("value", $value);
        return $element;
    }

    public static function form($text, $action = "", $method = "post", array $attr = array())
    {
        $element = new self("form", false, $attr);
        $element->setText($text, true);
        $element->setAttr("action", $action);
        $element->setAttr("method", $method);
        return $element;
    }

    public static function hiddenInput($name, $value, array $attr = array())
    {
        $element = new self("input", false, array_merge(array("type" => "hidden"), $attr));
        $name && $element->setAttr("name", $name);
        $value && $element->setAttr("value", $value);
        return $element;
    }

    public static function submitInput($name, $value, array $attr = array())
    {
        $element = new self("input", false, array_merge(array("type" => "submit"), $attr));
        $name && $element->setAttr("name", $name);
        $value && $element->setAttr("value", $value);
        return $element;
    }

    public static function button($text, array $attr = array())
    {
        $element = new self("button", $text, $attr);
        $element->setAttr("type", "button");
        return $element;
    }

    public static function a($href, $text, array $attr = array(), $rawText = false)
    {
        $attr["href"] = $href ? $href : "javascript:void(0)";
        return new self("a", $text, $attr, $rawText);
    }

    public static function checkbox($name, $title, $checked = false, $inputFirst = true, array $attr = array())
    {
        $id = "checkbox_" . rand();
        $defaultAttr = array("type" => "checkbox", "name" => $name, "value" => "1", "id" => $id);
        $input = new self("input", false, array_merge($defaultAttr, $attr));
        $label = new self("label", $title, array("for" => $id));

        if ($checked) {
            $input->setAttr("checked", "checked");
        }

        return $inputFirst ? $input . " " . $label : $label . " " . $input;
    }

    public static function img($src, $attr = array())
    {
        $attr['src'] = $src;
        return new self("img", false, $attr);
    }

    public static function script($code, array $attr = array())
    {
        return new self("script", $code, $attr);
    }

    public static function tabs(array $tabs)
    {
        $nav = false;
        $body = false;
        $visibleSet = false;

        foreach ($tabs as $name => $contents) {
            $id = "tab_" . mt_rand();
            $a = Element::a("#$id", $name, array("onclick" => "return tabs.show(this)"));
            $div = Element::div($contents, array("id" => $id, "class" => "tabPage"));

            if (!$visibleSet) {
                $a->addClass("tabVisible");
                $div->addClass("tabVisible");
                $visibleSet = true;
            }

            $nav .= $a;
            $body .= $div;
        }

        return (string)Element::div(
            $nav . $body . Element::script("tabs.init(this)"),
            array("class" => "tabFrame")
        );
    }

    public static function buttonProgress()
    {
        return self::img(
            URL_PLATFORM_IMAGES . "ajaxProgressButton.gif",
            array("class" => "buttonProgress", "style" => "display:none")
        );
    }

    public static function buttonBar($buttons, $big = false)
    {
        $buttons .= self::buttonProgress();

        $bar = new self("div", $buttons, array("class" => "buttonBar"));

        if ($big) {
            $bar->addClass("big");
        }

        return $bar;
    }


    /**
     * Sets an html attribute of the element.
     *
     * @param string $attribute Attribute name.
     * @param string $value     Attribute value;
     *
     * @return Object    Returns the current element object.
     */
    public function setAttr($attribute, $value = "")
    {
        $this->data[$attribute] = $value;
        return $this;
    }

    public function attr($attribute)
    {
        return $this->attrExists($attribute) ? $this->data[$attribute] : null;
    }

    /**
     * Checks if an attribute exists in the element attribute list.
     *
     * @param string $attribute Attribute name.
     *
     * @return bool
     */
    public function attrExists($attribute)
    {
        return array_key_exists($attribute, $this->data);
    }

    /**
     * Removes an attribute from the element attribute list.
     *
     * @param string $attribute Attribute name.
     *
     * @return Object Returns the current element object.
     */
    public function removeAttr($attribute)
    {
        unset($this->data[$attribute]);
        return $this;
    }

    /**
     * Removes all the attributes of the element.
     *
     * @return Object Returns the current element object.
     */
    public function clearAttributes()
    {
        $this->data = array();
        return $this;
    }

    /**
     * Inserts an html element to be placed inside the current element.
     *
     * @param string $htmlElement
     *
     * @return Object    Returns the current element object.
     */
    public function insertElement($htmlElement)
    {
        $this->text = $htmlElement;
        $this->htmlText = true;
        return $this;
    }

    /**
     * Adds a class to the element's class attribute.
     *
     * @param string $class The class to add;
     *
     * @return Object    Returns the current element object.
     */
    public function addClass($class)
    {
        if (!$class) {
            return $this;
        }

        $this->setAttr("class", $this->attrExists("class") ? $this->attr("class") . " $class" : $class);
        return $this;
    }

    /**
     * Adds a style to the element's style attribute.
     *
     * @param string $style The style string to add;
     *
     * @return Object    Returns the current element object.
     */
    public function addStyle($style)
    {
        if (!$style) {
            return $this;
        }

        $this->setAttr("style", $this->attrExists("style") ? $this->attr("style") . ";$style" : $style);
        return $this;
    }

    /**
     * Returns the html code of the element
     *
     * @return string
     */
    public function output()
    {
        // make sure we have all the parts to create the element

        if (!$this->type) {
            $this->setAttr("type", "div");
        }

        if ($this->type == "a" && !$this->attrExists("href")) {
            $this->setAttr("href", "javascript:void(0)");
        }

        if ($this->type == "img" && !$this->attrExists("src")) {
            $this->setAttr("src");
        }

        if ($this->type == "img" && !$this->attrExists("alt")) {
            $this->setAttr("alt");
        }

        if ($this->type == "input" && !$this->attrExists("name")) {
            $this->setAttr("name");
        }

        if ($this->type == "input" && !$this->attrExists("value")) {
            $this->setAttr("value");
        }

        if ($this->type == "input" && !$this->attrExists("type")) {
            $this->setAttr("type", "text");
        }

        if ($this->type == "input" && $this->attr("type") == "text" && !$this->attrExists("maxlength")) {
            $this->setAttr("maxlength", 250);
        }

        // collect attributes into a string

        $attr = "";

        foreach ($this->data as $key => $val) {
            $key = htmlentities($key, ENT_QUOTES, "UTF-8");
            $val = str_replace('"', "&quot;", $val);

            if ($key) {
                $attr .= " $key=\"$val\"";
            }
        }

        // encode the type and text (if text is not an html element)

        $type = htmlentities($this->type, ENT_QUOTES, "UTF-8");

        if ($this->rawText || $this->htmlText || $type == "script") {
            $text = $this->text;
        } else {
            $text = htmlentities($this->text, ENT_QUOTES, "UTF-8");
        }

        // return the element string

        $selfClosing = in_array($type, array("br", "hr", "img", "input", "link", "meta", "basefont", "base", "area"));

        return "<$type$attr" . ($selfClosing ? " />" : ">$text</$type>");
    }
}
