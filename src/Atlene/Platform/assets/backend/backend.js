/**
 * After making any changes to this file, publish it (copy it to the public directory) using this command:
 * php artisan vendor:publish --tag=public --force
 */

$(document).ready(function() {
    platform.initialize();
    sidebar.initialize();
    dialog.initialize();    
});

var platform = new function()
{
    this.initialize = function()
    {
        $(".btn.submit").click(function() { platform.showButtonProgress(this) });

        // adjust layout
        $("#container").css("top", $("header.navbar").outerHeight());
  
        // keep session alive on pages with token to prevent csrf mismatch errors
        setInterval(
            function() { $.ajax({ url: ksaUrl, type: 'GET' }) },
            900000 // 15 minutes
        );
    }
    
    this.showButtonProgress = function(element)
    {
        // using timeout otherwise submit buttons will be disabled before they get to submit the form
        setTimeout(function() { $(element).addClass("active").attr("disabled", "disabled"); }, 0);
    }
    
    this.hideButtonProgress = function()
    {
        $(".btn.submit.active").removeClass("active").removeAttr("disabled");
    }
    
    this.setColor = function(element, ajaxUrl)
    {
        var body = $("body");
        var element = $(element);
        var color = element.attr("data-color");

        body.removeClass(function(index, cls) { 
            return (cls.match(/color-\S+/g) || []).join(' ');
        });
        body.addClass("color-" + color);
        $(".backend-color-settings a").removeClass("active");
        element.addClass("active");

        $.ajax({
            url: ajaxUrl,
            type: 'POST',
            data: { _token: csrfToken, backendColor: color },
            success: function(result) {
                if (result.status != "ok") {
                    notification.show(result.message, "danger", 0);
                }
            }
        });
    }
    
    this.setGradient = function(event, state, ajaxUrl)
    {
        var checked = state ? 1 : 0;
        var body = $("body");

        if (checked) {
            body.addClass("gradient");
        } else {
            body.removeClass("gradient");
        }

        $.ajax({
            url: ajaxUrl,
            type: 'POST',
            data: { _token: csrfToken, backendGradient: checked },
            success: function(result) {
                if (result.status != "ok") {
                    notification.show(result.message, "danger", 0);
                }
            }
        });
    }
}

var sidebar = new function()
{
    this.initialize = function()
    {
        $("#sidebar-search input").keyup(function() { sidebar.search() });
    }
    
    this.toggleMenu = function(side)
    {
        var sidebar = $(".sidebar." + side);
        
        if (sidebar.width() > 100) {        
            $("body").addClass("sidebar-" + side + "-minimized");
            Cookies.set("minimized_" + side, 1, { path: "/", expires: 365 });
        } else {
            $("body").removeClass("sidebar-" + side + "-minimized");
            Cookies.remove("minimized_" + side, { path: "/" });
        }
    }    
    
    this.toggleItem = function(element, routeId)
    {
        if ($("body.minimized_left").length) {
            return;
        }
        
        var element = $(element);
        var menu = element.next(".list-group");
        
        if (!menu.length) {
            return;
        }

        if (menu.is(":visible")) {
            menu.slideUp(50, function() { 
                element.removeClass("open");
                menu.removeClass("open");
            });
            Cookies.remove("open-item-" + routeId, { path: "/" });
        } else {
            menu.slideDown(50, function() { 
                element.addClass("open");
                menu.addClass("open");
            });
            Cookies.set("open-item-" + routeId, 1, { path: "/", expires: 365 });
        }
    }

    this.search = function()
    {
        var text = $("#sidebar-search input").val().toLowerCase();
        $("#sidebar-results").html("");
        
        if (!text) {
            $("#sidebar-results").hide();
            $("#sidebar-empty").hide();
            $(".sidebar.left .root").show();
            return;
        }

        var items = "";
        
        $(".sidebar.left a.list-group-item").each(function() {
            var element = $(this);
            var href = element.attr("href");
            var title = element.text();
            
            
            if (href != "javascript:void(0)" && title.toLowerCase().indexOf(text) > -1) {
                items += "<a href='" + href + "' class='list-group-item'>" + title + "</a>";
            }
        });
        
        if (items) {
            $("#sidebar-results").show();
            $("#sidebar-results").html(items).show(); 
        } else {
            $("#sidebar-results").hide();
            $("#sidebar-empty").show();
        }
        
        $(".sidebar.left .root").hide();
    }
}

var crud = new function()
{
    this.save = function(crudId, method)
    {
        var form = $("#crud-form-" + crudId);
        var visibleTab = form.find(".nav-tabs > li.active > a").attr("id");
        
        form.ajaxSubmit({
            success: function(result) { 
                notification.hideAll();
                platform.hideButtonProgress();
                
                if (result.status == "ok") {
                    if (result.redirect) {
                        window.location = result.redirect;
                    } else {
                        $("#crud-edit-" + crudId).replaceWith(result.content);
                        notification.show(result.message, "success");
                    }
                    
                    // restore the tab that was visible before save
                    if (visibleTab) {
                        $("#" + visibleTab).tab("show");
                    }
                    
                    return false;
                }
                
                if (result.status == "error" && result.errors !== undefined) {

                    // clear any previous error markup and get new error information
                    form.find(".has-error").removeClass("has-error");
                    console.log(result.errors);
                    var errors = $.parseJSON(result.errors)

                    // mark input rows with error classes and add error messages to them
                    for (var name in errors) {
                        var element = form.find("#row-" + name);
                        if (element.length) {
                            element.addClass("has-error");
                            element.find(".error").text($.isArray(errors[name]) ? errors[name].join(" ") : errors[name]);
                        }
                    }
                    
                    // mark tabs with errors and show the first tab with error
                    
                    if (form.find(".nav-tabs").length) {
                        var shown = false;
                        
                        for (var name in errors) {
                            var element = form.find("#row-" + name);
                            if (element.length) {
                                var pane = element.parents(".tab-pane");
                                if (pane.length) {
                                    var tabLink = form.find("#" + pane.attr("id").replace("tab", "tab-link"));
                                    tabLink.addClass("has-error");
                                    
                                    if (!shown) {
                                        tabLink.tab('show');
                                        shown = true;
                                    }
                                }
                            }
                        }
                    }
                }

                // show page error notification
                notification.show(result.message === undefined ? "Unknown error" : result.message, "danger", 0);
            }
        });
        
        return false;
    }
    
    
    this.destroy = function(targetUrl, crudId)
    {
        if (!confirm("Are you sure you want to delete this item?")) {
            return;
        }
        
        $.ajax({
            url: targetUrl,
            type: 'DELETE',
            data: { _token: csrfToken },
            success: function(result) {
                if (result.status == "ok") {
                    $("#crud-index-" + crudId).replaceWith(result.content);
                    notification.show(result.message);
                    return;
                }
                
                if (result.message === undefined) {
                    result.message = "Unknown error";
                }
                
                notification.show(result.message, "danger", 0);
            }
        });        
    }
    
    this.showFilters = function(crudId, dialogTitle, searchButton, resetButton, resetUrl)
    {
        var container = $("#crud-index-" + crudId + " .crud-filter-container");
        
        bootbox.dialog({
            title: dialogTitle,
            message: container.html(), 
            buttons: {
                search: {
                    label: searchButton,
                    className: "btn-primary",
                    callback: function() { container.find("form").submit(); }
                },
                reset: {
                    label: resetButton,
                    className: "btn-default",
                    callback: function() { window.location = resetUrl; }
                }
            }
        });
    }
}

var notification = new function()
{
    /*
     * types: success, info, warning, danger
     * timeout: how long to keep the notification displayed before it's hidden (in seconds)
     * specify 0 to disable auto-hide 
     */  
    this.show = function(message, alertType, timeout)
    {
        alertType = alertType !== undefined ? alertType : "info";
        timeout = timeout !== undefined ? timeout : 5;
        
        var button = "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
        var element = $("<div/>").attr("class", "alert alert-dismissible alert-" + alertType).html(message + button);
        element.hide();
        element.appendTo("#notifications");
        element.fadeIn();

        if (timeout) {
            setTimeout(function() { element.fadeOut(500, function() { element.remove() }) }, timeout * 1000);
        }
    }
    
    this.hideAll = function()
    {
        $("#notifications").html("");
    }
}

var dialog = new function()
{
    var dragElement = false;
    var dragOffsetX = false;
    var dragOffsetY = false;
    
    this.initialize = function()
    {
        // set up dialong dragging
        
        $(document.body).on("mousedown", ".dialog .dialog-title", function(event) {
            // drag only on left click
            if (event.which == 1) {
                dialog.dragElement = $(event.target).parents(".dialog");
                dialog.dragOffsetX = event.pageX - dialog.dragElement.offset().left;
                dialog.dragOffsetY = event.pageY - dialog.dragElement.offset().top;
                event.preventDefault();
            }
        });

        $(document.body).on("mouseup", function(event) {
            dialog.dragElement = false;
        });
        
        $(document.body).on("mousemove", function(event) {
            if (dialog.dragElement) {
                dialog.dragElement.offset({ 
                    left: event.pageX - dialog.dragOffsetX, 
                    top: event.pageY - dialog.dragOffsetY
                });
            }
        });
    }
    
    this.hide = function(id)
    {
        this.getBox(id).fadeOut();
        $("#dialog-mask").fadeOut();
    }
    
    this.show = function(id)
    {
        var box = this.getBox(id);
        var mask = $("#dialog-mask");
        
        // create the mask element if it doesn't exist
        if (!mask.length) {
            $("body").append("<div id='dialog-mask'></div>");
        }

        // add the dialog class to box if it doesn't exist
        box.addClass("dialog");
        
        // create the dialog title if it doesn't exist
        if (!box.find(".dialog-title").length) {
            box.prepend("<div class='dialog-title bg-primary'>&nbsp;</div>");
        }

        // add the close button to the title bar if it doesn't exist
        if (!box.find(".dialog-title a").length) {
            box.find(".dialog-title").prepend(
                "<a href='javascript:void(0)' onclick='dialog.hide(\"" + id + "\")'><i class='fa fa-times'></i></a>"
            );
        }

        // create the dialog content if it doesn't exist
        var content = box.find(".dialog-content");
        if (!content.length) {
            box.prepend("<div class='dialog-content'></div>");
            var content = box.find(".dialog-content");
        }                

        var buttons = box.find(".dialog-buttons");
        
        if (buttons.length) {
            content.css("bottom", buttons.outerHeight());
            
            // reverse buttons on windows
            if (navigator.platform.indexOf("Win") != -1) {
                buttons.addClass("windows");
            }
        }
        
        // show dialog
        mask.fadeIn('fast');
        box.fadeIn('fast');
    }
    
    this.make = function(id, title, content, buttons)
    {
        var box = this.getBox(id);

        // remove box if exists
        if (box.length) {
            box.remove();
        }
        
        if (typeof title == "undefined" || !title) {
            title = "&nbsp;";
        }
        
        if (typeof content == "undefined") {
            content = this.getLoaderCode();
        }
        
        $("body").append(
            "<div id='" + id + "' class='dialog'>" + 
                "<div class='dialog-title bg-primary'>" + title + "</div>" +
                "<div class='dialog-content'>" + content + "</div>" +
                (buttons ? "<div class='dialog-buttons'>" + buttons + "</div>" : "") +
            "</div>"
        );        
    }
    
    this.setContent = function(id, content)
    {
        var box = this.getBox(id);
        
        if (box.length) {
            box.find(".dialog-content").html(content);
        }
    }
    
    this.insertLoader = function(id)
    {
        this.setContent(id, this.getLoaderCode()); 
    }
    
    this.getLoaderCode = function()
    {
        return "<div class='dialog-progress'><i class='fa fa-circle-o-notch fa-spin'></i></div>";
    }
    
    /**
     * Takes an id and returns jquery element. Id can but doesn't have to start with a #.
     */ 
    this.getBox = function(id)
    {
        if (id.substr(0, 1) == "#") {
            id = id.substr(1);
        }     
        
        return $("#" + id);
    }
}

var access = new function()
{
    this.setAll = function(value)
    {
        $(".permissions-table :radio[value='" + value + "']").parent().click()
    }
    
    this.change = function(element)
    {
        switch ($(element).val()) {
            case "0":
                var classAttr = "permission-0 bg-danger";
                break;
            case "2":
                var classAttr = "permission-1 bg-success";
                break;
            default:
                var classAttr = "permission-1";
        }

        // permission names contain periods, so we need to select the row using this notation instead of the
        // standard $("#row...")
        $("tr[id='row-" + $(element).attr("name") + "']").attr("class", classAttr);

        var object = {};
        $(".permissions-table input[type=radio]:checked").each(function() {
            object[$(this).attr("name")] = $(this).val();
        });
        $("#permissions").val(JSON.stringify(object));
    }
}