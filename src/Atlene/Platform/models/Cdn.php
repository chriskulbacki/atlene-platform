<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use File;
use App;
use Intervention\Image\ImageManagerStatic;

class Cdn extends \Illuminate\Routing\Controller
{
    const FIT = "f";
    const RESIZE = "r";

    public static function getStoragePath($additionalPath = false)
    {
        return storage_path("atlene/cdn") . ($additionalPath ? DIRECTORY_SEPARATOR . $additionalPath : "");
    }

    public static function getPublicPath($additionalPath = false)
    {
        return public_path("atlene/cdn") . ($additionalPath ? DIRECTORY_SEPARATOR . $additionalPath : "");
    }

    public static function makeDirectory($path)
    {
        $umask = umask(0);
        File::makeDirectory($path, 0777, true, true);
        umask($umask);
    }

    /**
     * Returns the url to the version of the image that is resized to fit.
     *
     * If the image doesn't exist when the browser requests it, the cdn.image route is triggered, Cdn::createImage()
     * creates the image and outputs it using Cdn::outputImage(). On subsequent requests, the image is served directly.
     *
     * @param string $path
     * @param int $width
     * @param int $height
     * @param string $position
     * @param bool $upsize
     */
    public static function getFitImageUrl($path, $width = 100, $height = 100, $position = "center", $upsize = false)
    {
        $param = [self::FIT, $width, $height];
        $position == "center" || $param[] = $position;
        $upsize == false || $param[] = 1;

        return self::getImageUrl($param, $path);
    }

    /**
     * Returns the url to a resized version of the image in the public folder.
     *
     * If the image doesn't exist when the browser requests it, the cdn.image route is triggered, Cdn::createImage()
     * creates the image and outputs it using Cdn::outputImage(). On subsequent requests, the image is served directly.
     *
     * @param string $path
     * @param int $width
     * @param int $height
     * @param bool $aspectRatio
     * @param bool $upsize
     */
    public static function getResizedImageUrl($path, $width = 100, $height = 100, $aspectRatio = true, $upsize = false)
    {
        $param = [self::RESIZE, $width, $height];
        $aspectRatio == true || $param[] = 0;
        $upsize == false || $param[] = 1;

        return self::getImageUrl($param, $path);
    }

    /**
     * Creates the image file in the public directory on first access. The process is as follows:
     *
     * 1. The image is uploaded to the storage folder, for example, using a crud form.
     * 2. The image is requested to be displayed using, for example, Cdn::getFitImageUrl(), which generates
     *    the url to a non-existing image in the public folder. The url includes the encoded parameters and the
     *    path to the image that is exactly the same as the path to the image in the storage folder.
     * 3. The file in the public folder doesn't exist, so the routes catches the url and executes this function.
     * 4. This function copies the image from the storage folder to the public folder, applying all the image
     *    manipulation specified in the encoded parameters. It then outputs the image using Cdn::outputImage()
     * 5. On the subsequent requests, the image file will exist in the public folder, so it will be displayed
     *    directly, it will not trigger the routes any more.
     *
     * @param string $encodedParam
     * @param string $file
     * @throws \Exception
     */
    public static function createImage($encodedParam, $file)
    {
        try {
            // decode the encoded url string to get the parameters

            if (!($param = self::decodeParameters($encodedParam))) {
                throw new \Exception();
            }

            if (!is_array($param) || empty($param)) {
                throw new \Exception();
            }

            // set the parameters to the proper keys depending on the requested action

            if ($param[0] == self::FIT && count($param) >= 3) {
                $param = [
                    "action" => $param[0],
                    "width" => $param[1],
                    "height" => $param[2],
                    "position" => array_key_exists(3, $param) ? $param[3] : "center",
                    "upsize" => array_key_exists(4, $param) ? true : false,
                ];
            } elseif ($param[0] == self::RESIZE && count($param) >= 3) {
                $param = [
                    "action" => $param[0],
                    "width" => $param[1],
                    "height" => $param[2],
                    "aspectRatio" => array_key_exists(3, $param) ? false : true,
                    "upsize" => array_key_exists(4, $param) ? true : false,
                ];
            } else {
                throw new \Exception();
            }

            // perform the operation on the image

            $source = self::getStoragePath($file);
            $target = self::getPublicPath("$encodedParam/$file");

            // documentation: http://image.intervention.io/getting_started
            $image = ImageManagerStatic::make($source);

            if ($param['action'] == self::FIT) {
                $image->fit(
                    $param['width'],
                    $param['height'],
                    function ($constraint) use ($param) {
                        $param['upsize'] || $constraint->upsize();
                    },
                    $param['position']
                );
            } elseif ($param['action'] == self::RESIZE) {
                $image->resize(
                    $param['width'] ? $param['width'] : null,
                    $param['height'] ? $param['height'] : null,
                    function ($constraint) use ($param) {
                        !$param['aspectRatio'] || $constraint->aspectRatio();
                        $param['upsize'] || $constraint->upsize();
                    }
                );
            } else {
                throw new \Exception();
            }

            // make the needed directories and save the image

            self::makeDirectory(dirname($target));
            $image->save($target);

        } catch (\Exception $e) {
            App::abort(404);
        }

        // send the saved image to the browser

        self::outputImage($target);
    }

    /**
     * Creates the url to the image file, includes the parameters and the path to the image.
     *
     * @param array $param
     * @param string $path
     */
    protected static function getImageUrl(array $param, $path)
    {
        return route("cdn.image", ["param" => self::encodeParameters($param), "path" => $path]);
    }

    /**
     * Encodes the array of image manipulation properties (width, height, aspect ratio, etc.) into a single
     * string that is then used in the url.
     *
     * @param array $data
     */
    protected static function encodeParameters(array $data)
    {
        !is_array($data) || $data = implode("|", $data);
        is_string($data) || $data = (string)$data;
        return bin2hex(gzdeflate($data, 9));
    }

    /**
     * Decodes the encoded strings of image manipulation parameters (width, height, aspect ratio, etc.)
     *
     * @param string $string
     */
    protected static function decodeParameters($string)
    {
        $result = gzinflate(hex2bin($string));
        return strpos($result, "|") == false ? $result : explode("|", $result);
    }

    /**
     * Sends an image to the browser.
     *
     * @param string $file
     */
    protected static function outputImage($file)
    {
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        switch ($ext) {
            case "png":
                header("Content-type: image/png");
                break;
            case "jpg":
            case "jpe":
            case "jpeg":
                header("Content-type: image/jpeg");
                break;
            case "gif":
                header("Content-type: image/gif");
                break;

            /* these formats could be supported by switching intervaention/image from GD to Imagick

            case "bmp":
                header("Content-type: image/bmp");
                break;
            case "tif":
            case "tiff":
                header("Content-type: image/tiff");
                break;
            case "ico":
                header("Content-type: image/x-icon");
                break;

            /* these formats are not supported by GD or Imagick

            case "cod":
                header("Content-type: image/cis-cod");
                break;
            case "ief":
                header("Content-type: image/ief");
                break;
            case "jfif":
                header("Content-type: image/image/pipeg");
                break;
            case "svg":
                header("Content-type: image/svg+xml");
                break;
            case "ras":
                header("Content-type: image/x-cmu-raster");
                break;
            case "cmx":
                header("Content-type: image/x-cmx");
                break;
            case "pnm":
                header("Content-type: image/x-portable-anymap");
                break;
            case "pbm":
                header("Content-type: image/x-portable-bitmap");
                break;
            case "pgm":
                header("Content-type: image/x-portable-graymap");
                break;
            case "ppm":
                header("Content-type: image/x-portable-pixmap");
                break;
            case "rgb":
                header("Content-type: image/x-rgb");
                break;
            case "xbm":
                header("Content-type: image/x-xbitmap");
                break;
            case "xpm":
                header("Content-type: image/x-xpixmap");
                break;
            case "xwd":
                header("Content-type: image/x-xwindowdump");
                break;
            */

            default:
                header("Content-type: text/plain");
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Length: " . filesize($file));

        exit(@readfile($file));
    }
}