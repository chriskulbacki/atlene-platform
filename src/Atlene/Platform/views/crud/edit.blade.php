<?php
    $formButton = Form::button(
        tr('Save'),
        [
            'class' => 'btn btn-primary btn-save submit',
            "onclick" => "crud.save('$crudId', '" . ($id ? "PUT" : "POST") . "')"
        ]
    );
?>

<div id='crud-edit-{{$crudId}}' class='crud-edit crud'>

    {!! Form::model(
            $record,
            [
                "url" => $targetUrl,
                "method" => ($id ? "PUT" : "POST"),
                "class" => "form-horizontal",
                "id" => "crud-form-$crudId",
                "autocomplete" => "off",
            ]
        ); !!}

        {!! Form::token() !!}

        <div class="crud-form-buttons top-buttons">{!! $formButton !!}</div>

        <?php // prevent autocomplete in chrome ?>
        <input style="display:none" type="text" name=""/>
        <input style="display:none" type="password" name=""/>

        @if (count($tabs) > 1)
            <ul class="nav nav-tabs">
                @foreach ($tabs as $key => $tabData)
                    @if (!empty($tabData['fields']))
                        <li role="presentation"{!! $tabData['active'] ? ' class="active"' : '' !!}>
                            <a href="#tab-{{$crudId}}-{{$key}}" id="tab-link-{{$crudId}}-{{$key}}"
                                aria-controls="tab-{{$crudId}}-{{$key}}" role="tab" data-toggle="tab">{{ $tabData['title'] }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        @endif

        <div class="tab-content">
            @foreach ($tabs as $key => $tabData)
                <div role="tabpanel" class="tab-pane {{$tabData['active'] ? ' active' : ''}}" id="tab-{{$crudId}}-{{$key}}">
                    @foreach ($tabData['fields'] as $field)
                        <?php
                            $label = $fields[$field]['title'][$action];
                            $inline = $fields[$field]['input'][$action]['inline'];
                            $type = $fields[$field]['input'][$action]['type'];
                            $options = $fields[$field]['input'][$action]['options'];
                            $value = $fields[$field]['value'];

                            $attr = $fields[$field]['input'][$action]['attr'];
                            if (array_key_exists("class", $attr)) {
                                $attr['class'] .= " form-control";
                            } else {
                                $attr['class'] = "form-control";
                            }
                        ?>
                        <div class="form-group" id='row-{{$field}}'>
                            @if ($label)
                                {!! Form::label($field, $label, array("class" => "col-sm-" . ($inline ? "2" : "12 block-label") . " control-label")) !!}
                            @endif
                            <div class="col-sm-{{ $inline ? '10' : '12' }} input-{{$type}}">
                                @if ($type == "custom")
                                    {!! $value !!}
                                @elseif ($type == "checkbox")
                                    {{ $attr['class'] = str_replace("form-control", "", $attr['class']) }}
                                    {!! Form::checkbox($field, 1, $value, $attr) !!}
                                @elseif ($type == "date")
                                    {!! Form::dateinput($field, $value, $options, $attr) !!}
                                @elseif ($type == "time")
                                    {!! Form::timeinput($field, $value, $options, $attr) !!}
                                @elseif ($type == "datetime")
                                    {!! Form::datetimeinput($field, $value, $options, $attr) !!}
                                @elseif ($type == "email")
                                    {!! Form::email($field, $value, $attr) !!}
                                @elseif ($type == "image")
                                    {!! Form::imageinput($field, $value, $record, $options, $attr) !!}
                                @elseif ($type == "file")
                                    {!! Form::fileinput($field, $value, $record, $options, $attr) !!}
                                @elseif ($type == "password")
                                    {!! Form::password($field, $attr) !!}
                                @elseif ($type == "select")
                                    {!! Form::select($field, is_array($options) ? $options : [], $value, $attr) !!}
                                @elseif ($type == "text")
                                    {!! Form::text($field, $value, $attr) !!}
                                @elseif ($type == "textarea")
                                    {!! Form::textarea($field, $value, $attr) !!}
                                @elseif ($type == "number")
                                    {!! Form::number($field, $value, $attr) !!}
                                @elseif ($type == "range")
                                    {!! Form::selectRange($field, $options[0], $options[1], $value, $attr) !!}
                                @elseif ($type == "month")
                                    {!! Form::selectMonth($field, $value, $options, $format) !!}
                                @endif

                                <div class='error text-danger'></div>
                                <?php // @include("atlene::form." . $type) ?>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>

        <div class="crud-form-buttons bottom-buttons">{!! $formButton !!}</div>

    {!! Form::close() !!}
</div>
