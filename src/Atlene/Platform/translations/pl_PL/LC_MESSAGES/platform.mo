��    6      �  I   |      �     �     �     �     �  
   �  
   �     �  
   �     �  %   �     %     A  -   Z     �     �     �     �  	   �     �     �     �     �     �  
   �  	   �     �  
                    s   "  	   �     �     �     �     �  
   �     �     �     �     �     �     �     �     �                         &     +     1     9  �  =     �     �     �     	     	  	   	  
   "	     -	     <	  $   N	  &   s	     �	  ;   �	     �	     

     
     !
     =
     C
  	   O
     Y
     _
     f
     m
     {
  	   �
     �
     �
     �
     �
  \   �
               '     -     3     7     H     M     T     `  	   f     p     w     �     �     �     �     �     �     �     �     �     5      *                   '       #      
       .                 (      &                       !   )                         ,   2                           -                /   4              %      0      6   "      +   1                  	      3   $    %s GB %s MB %s byte %s bytes %s kB %s: create %s: delete %s: read %s: update Access dashboard Allows access to the admin dashboard. Allows creating new record. Allows deleting records. Allows editing and updating existing records. Browse website Create Create group Create user Dashboard Date format Default Delete E-mail Edit Edit group Edit user English First name General Groups ID It has been over 30 days since you changed your account password. You must change your password before you proceed. Last name Log in again Login Name No No results Other Password Permissions Remove Reset Save Search Select Send Status Time format Translations User Users Website Yes Project-Id-Version: oxm
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-29 09:35+0200
PO-Revision-Date: 2015-08-29 09:36+0100
Last-Translator: Chris <chris@sprigo.com>
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 %s GB %s MB %s bajt %s bajty %s bajtów %s kB %s: utwórz %s: usuń %s: czytaj %s: aktualizuj Dostęp do panelu Zezwól na dostęp do panelu admina. Pozwala na tworzenie nowych rekordów. Pozwala na usuwanie rekordów. Pozwala na edycję i aktualizację istniejących rekordów. Przeglądaj witrynę Utwórz Utwórz grupę Utwórz nowego użytkownika Panel Format daty Domyślne Usuń E-mail Edytuj Edytuj grupę Edytuj użytkownika angielski Imię Ogólne Grupy ID Minęło 30 dni od zmiany Twojego hasła. Ze względów bezpieczeństwa zmień swoje hasło. Nazwisko Zaloguj ponownie Login Nazwa Nie Brak rezultatów Inne Hasło Uprawnienia Usuń Wyczyść Zapisz Wyszukiwanie Wybierz Wyślij Status Format czasu Tłumaczenia Użytkownik Użytkownicy Witryna Tak 