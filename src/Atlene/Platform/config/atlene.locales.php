<?php

return [

    1 => [
        "code" => "en",
        "language" => "en_US",
        "name" => "English",
        "translated_name" => _("English"),
        "currency_format" => "$#,###.##",
        "enabled" => true,
    ],

];
