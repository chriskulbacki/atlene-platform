<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use DB;
use App;
use Auth;

class AccessCode extends Model
{
    private $allPermissions = [];
    private $userPermissions = null;

    /**
     * Returns the permissions table.
     *
     * @return array:
     */
    public function getAllPermissions()
    {
        return $this->allPermissions;
    }

    /**
     * Returns the permissions of the currently logged in user.
     *
     * @return array
     */
    public function getUserPermissions()
    {
        if ($this->userPermissions === null) {
            if (Auth::check()) {
                $this->compileUserPermissions();
            } else {
                $this->userPermissions = [];
            }
        }

        return $this->userPermissions;
    }

    /**
     * Checks if the logged in user has the specified custom permission.
     *
     * @param string $key
     * @return boolean
     */
    public function hasPermission($key)
    {
        if (!Auth::check()) {
            return false;
        }

        if (Auth::user()->superuser || is_bool($key)) {
            return true;
        }

        $key = "#$key";

        return array_key_exists($key, $this->userPermissions) && $this->userPermissions[$key] == 2;
    }

    /**
     * Checks if the logged in user has the specified custom permission and aborts the application on false.
     *
     * @param string $key
     */
    public function verifyPermission($key)
    {
        $this->hasPermission($key) || App::abort(401);
    }

    /**
     * Checks if the logged in user has acccess to the specified route.
     *
     * @param string $route
     * @return boolean
     */
    public function isRouteAllowed($route)
    {
        if (!Auth::check()) {
            return false;
        }

        if (Auth::user()->superuser || is_bool($route)) {
            return true;
        }

        $allowed = false;
        foreach ($this->getUserPermissions() as $key => $val) {
            if (strpos($route, "#") !== 0 && preg_match("/$key/", $route)) {
                $allowed = (bool)$val;
            }
        }

        return $allowed;
    }

    /**
     * Adds a custom user permission to the permissions table. Custom permissions are automatically perpended with
     * the # character. They have to be verified manually using hasPermission() or verifyPermission().
     *
     * @param string $category
     * @param string $key
     * @param string $name
     * @param string $description
     */
    public function addPermission($category, $key, $name, $description)
    {
        $this->allPermissions[$category][] = ["key" => "#$key", "name" => $name, "description" => $description];
    }

    /**
     * Removes a custom permission from the permissions table.
     *
     * @param string $key
     */
    public function removePermission($key)
    {
        $this->removeRoute("#$key");
    }

    /**
     * Removes a route permission from the permissions table.
     *
     * @param string $route
     */
    public function removeRoute($route)
    {
        foreach ($this->allPermissions as $category => $items) {
            foreach ($items as $key => $item) {
                if ($item['key'] == $route) {
                    unset($this->allPermissions[$category][$key]);
                }
            }
        }

        // remove empty categories

        foreach ($this->allPermissions as $category => $items) {
            if (empty($items)) {
                unset($this->allPermissions[$category]);
            }
        }
    }

    /**
     * Adds a route permission to the permissions table. Route permissions are automatically verified
     * by the system. The permissions can include regular expressions to match a collection of routes.
     *
     * @param string $category
     * @param string $key
     * @param string $name
     * @param string $description
     */
    public function addRoute($category, $key, $name, $description)
    {
        $this->allPermissions[$category][] = ["key" => $key, "name" => $name, "description" => $description];
    }

    /**
     * Adds a set of route permissions matching a crud model.
     *
     * @param string $category
     * @param string $prefix
     * @param string $crudName Used to create permission names.
     */
    public function addCrud($category, $prefix, $crudName)
    {
        $this->addRoute(
            $category,
            $prefix . ".index",
            sprintf(tr("%s: read"), $crudName),
            tr("Allows displaying the list of records.")
        );
        $this->addRoute(
            $category,
            $prefix . ".(create|store)",
            sprintf(tr("%s: create"), $crudName),
            tr("Allows creating new record.")
        );
        $this->addRoute(
            $category,
            $prefix . ".(edit|update)",
            sprintf(tr("%s: update"), $crudName),
            tr("Allows editing and updating existing records.")
        );
        $this->addRoute(
            $category,
            $prefix . ".destroy",
            sprintf(tr("%s: delete"), $crudName),
            tr("Allows deleting records.")
        );
    }

    /**
     * Compiles group permissions and user permissions of the current user to create a list of finalized valid
     * permissions applicable to the user.
     *
     * @param User $user
     */
    private function compileUserPermissions()
    {
        $permissions = [];
        $user = Auth::user();

        foreach ($this->getAllPermissions() as $group => $items) {
            foreach ($items as $item) {
                $permissions[$item['key']] = 0;
            }
        }

        // set groups the user belongs to

        $groupIds = DB::table(Setting::get("table_users_groups"))->where("user_id", $user->id)->lists("group_id");

        if (empty($groupIds)) {
            $groups = [];
        } else {
            $groups = Group::whereIn("id", $groupIds)->lists("permissions", "id");
        }

        foreach ($groups as $json) {
            if ($json) {
                $array = json_decode($json, true);

                foreach ($permissions as $key => $access) {
                    if (array_key_exists($key, $array) && $array[$key] == 2) {
                        $permissions[$key] = 2;
                    }
                }
            }
        }

        if (!empty($user->permissions)) {
            $array = json_decode($user->permissions, true);

            foreach ($permissions as $key => $access) {
                if (array_key_exists($key, $array)) {
                    if ($array[$key] == 2) {
                        $permissions[$key] = 2;
                    } else if ($array[$key] == 0) {
                        $permissions[$key] = 0;
                    }
                }
            }
        }

        $this->userPermissions = $permissions;
    }
}