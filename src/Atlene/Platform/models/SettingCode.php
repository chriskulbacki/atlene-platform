<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Request;
use DB;
use Input;

class SettingCode extends \Atlene\Platform\Configuration
{
    protected $name = "atlene.settings";

    public function isDevDomain()
    {
        return in_array(Request::server('SERVER_NAME', false), $this->get("developer_domains")) || Input::get("dev");
    }

    public function fixConfig()
    {
        array_key_exists("backend_title", $this->config) || $this->config['backend_title'] = "Administration Panel";
        array_key_exists("admin_subdomain", $this->config) || $this->config['admin_subdomain'] = false;
        array_key_exists("developer_domains", $this->config) || $this->config['developer_domains'] = ['localhost'];
        array_key_exists("browser_cache_ttl", $this->config) || $this->config['browser_cache_ttl'] = 60;
        array_key_exists("translation_source_dir", $this->config) ||
            $this->config['translation_source_dir'] = [app_path(), config_path()];
        array_key_exists("translation_target_dir", $this->config) ||
            $this->config['translation_target_dir'] = app_path("Translations");

        is_array($this->config['developer_domains']) ||
            $this->config['developer_domains'] = [$this->config['developer_domains']];
        is_array($this->config['translation_source_dir']) ||
            $this->config['translation_source_dir'] = [$this->config['translation_source_dir']];
    }
}
