<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class ColorCode
{
    protected $defaultColor = "309ae3";
    protected $defaultGradient = true;
    protected $template = "3f3f3f";
    protected $colors = false;
    protected $styles = false;

    public function __construct()
    {
        $this->colors = include(__DIR__ . "/../resources/colors/colors.php");
        $this->styles = file_get_contents(__DIR__ . "/../resources/colors/colors.css");
    }

    /**
     * Get all the color names: the keys in the colors array.
     *
     * @return multitype:
     */
    public function getColorNames()
    {
        return array_keys($this->colors);
    }

    /**
     * Get the default color that will be used for all the new accounts.
     *
     * @return string
     */
    public function getDefaultColor()
    {
        return $this->defaultColor;
    }

    public function getDefaultGradient()
    {
        return $this->defaultGradient;
    }

    /**
     * Checks if the color name is valid and returns the default color if it's not.
     *
     * @param string $colorName
     */
    public function ensureColorName($colorName)
    {
        if (in_array($colorName, $this->getColorNames())) {
            return $colorName;
        }

        return $this->defaultColor;
    }

    /**
     * Generates the css file with all the color styles. Use the script in resources/colors to run this function
     * and create the file.
     */
    public function generateStyles()
    {
        $css = "";
        $keys = array_keys($this->colors[$this->template]);
        $target = realpath(__DIR__ . "/../assets/backend/colors.css");

        foreach ($this->colors as $name => $color) {

            // make sure all the keys are present

            if (!array_key_exists("sidebar", $color)) {
                $color['sidebar'] = $color['navbar'];
            }

            if (!array_key_exists("table", $color)) {
                $color['table'] = $color['navbar'];
            }

            foreach ($keys as $key) {
                if (!array_key_exists($key, $color)) {
                    $color[$key] = $this->colors[$this->template][$key];
                }
            }

            eval("\$output = \"$this->styles\";");
            $css .= str_replace("~", ".color-$name", $output);
        }


        file_put_contents($target, preg_replace('/\s\s+/', ' ', $css));

        exit("\nStyles saved in:\n\n$target\n\n");
    }
}

