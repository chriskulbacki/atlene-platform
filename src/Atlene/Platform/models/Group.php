<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use DB;

class Group extends Crud
{
    protected $table = "groups";

    public function __construct()
    {
        $this->table = Setting::get("table_groups");
    }

    public static function destroy($id)
    {
        DB::table(Setting::get("table_users_groups"))->where("group_id", $id)->delete();

        parent::destroy($id);
    }
}
