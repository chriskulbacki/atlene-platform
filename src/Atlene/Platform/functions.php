<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

/**
 * Replacement for gettext() to be used in atlene packages. It temporarily switches the gettext domain
 * to pull the translations from the correct mo file. The default domain is always set to "app", which contains
 * the application translations. All the app code should use the standard _() or gettext() syntax.
 *
 * This function also allows the inclusion of additional strings into the translation text with the
 * purpose of differentiating the strings that have different meanings, for example: order (as in product order)
 * and order (as in sort order). The additional strings should be added wrapped in < > (which is later stripped
 * before displaying the text.
 *
 * The translators should be instructed not to translate the text included in < >. Also, html tags must not be
 * passed to this function or they'll be stripped.
 *
 * To find all instances of this function in the code, use: [^\w]tr\(
 *
 * @param string $string
 * @return string
 */
function tr($msgid, $domain = "platform")
{
    return trim(strip_tags(dgettext($domain, $msgid)));
}

function trn($msgid1, $msgid2, $n, $domain = "platform")
{
    return trim(strip_tags(dcngettext($domain, $msgid1, $msgid2, $n)));
}

/**
 * This function can appends the file timestamp at the end of url returned by asset(). The added parameters
 * makes sure the file is re-loaded in the browser after it's been modified on the server.
 *
 * @param string $asset
 * @return string|boolean
 */

function assetStamp($asset)
{
    try {
        return asset($asset) . "?q=" . crc32("q" . filemtime(public_path($asset)));
    } catch (\Exception $e) {
        return false;
    }
}

function override($key)
{
    if (class_exists("\Atlene\Platform\Override")) {
        return \Atlene\Platform\Override::get($key);
    }

    return false;
}
