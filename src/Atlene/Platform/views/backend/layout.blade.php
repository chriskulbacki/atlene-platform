<!DOCTYPE html>
<html lang="{{ $currentLocale['code'] }}">
<head>
    <title>{{ $mainTitle }}</title>
    @include("platform::backend.head")
</head>

<body class='{{ $bodyClasses }}'>

    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">

        @if ($leftMenu)
        <div class="navbar-header pull-left menu-toggle sidebar-left-toggle">
            <a class="navbar-brand" href='javascript:void(0)' onclick="sidebar.toggleMenu('left')"><i class="fa fa-bars"></i></a>
        </div>
        @endif

        @if ($rightMenu)
            <div class="navbar-header pull-right menu-toggle sidebar-right-toggle">
                <a class="navbar-brand" href='javascript:void(0)' onclick="sidebar.toggleMenu('right')"><i class="fa fa-chevron-right"></i></a>
            </div>
        @endif

        <div class="navbar-header pull-right go-to-website">
            <a class="navbar-brand" href='{{ $websiteUrl }}' title='{{ tr("Go to website") }}'><i class="fa fa-desktop"></i></a>
        </div>

        <div class="navbar-header pull-left">
            <a class="navbar-brand" href="{{ $dashboardRoute }}">{{ $mainTitle }}</a>
        </div>
        <ul class="nav navbar-nav pull-right toolbar">
            {!! $topMenu !!}

            @if ($locales && count($locales) > 1)
               <li class="dropdown">
                    <a class="dropdown-toggle locale" data-toggle="dropdown" href="javascript:void(0)">
                        <span>{{ $currentLocale['name'] }}</span>
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        @foreach ($locales as $locale)
                        <li>
                            <a href="{{ $locale['url'] }}">
                                <span>{{ $locale['name'] }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
            @endif

            <li class="dropdown user">
                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                    <img src='{{ $avatar }}' class='avatar' alt='' />
                    <span>
                        {{ $user->first_name }} {{ $user->last_name }}
                    </span>
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    @if ($preferences)
                    <li>
                        <a href="{{ $preferencesRoute }}">
                            <span>{{ tr("Preferences") }}</span>
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ $logoutRoute }}">
                            <span>{{ tr("Logout") }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="navbar-brand dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-th-large"></i>
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu color-select">
                    <div class='backend-color-settings clearfix'>
                        @foreach ($colors as $color)
                            <a href='javascript:void(0)' data-color='{{ $color }}' class='color{{ $backendColor == $color ? " active" : "" }}'>
                                <span style='background-color: #{{$color}}'></span>
                            </a>
                        @endforeach
                    </div>
                    <div class='backend-gradient-settings'>
                        <label for='backend-gradient'>{{ tr("Use gradients:") }}</label>
                        <input
                            type='checkbox'
                            id='backend-gradient'
                            data-size='small'
                            data-on-text='{{ tr("Yes") }}'
                            data-off-text='{{ tr("No") }}'
                            {{ $backendGradient ? "checked='checked'" : "" }}
                        />
                    </div>
                    <script>
                        $(".backend-color-settings a").on("click", function() { platform.setColor(this, "{{ $colorRoute }}") });
                        $("#backend-gradient").bootstrapSwitch({ onSwitchChange: function(event, state) {
                            platform.setGradient(event, state, "{{ $colorRoute }}");
                        }});
                    </script>
                </div>
            </li>
        </ul>
    </header>

    <div id="container" class="container-fluid">

        @if ($leftMenu)
        <div class='sidebar left navbar navbar-default'>
            <div id='sidebar-search' class='list-group'>
                <div class='list-group-item'>
                    <i class="fa fa-search"></i>
                    <input type='text' name='sidebar-search' value='' placeholder='{{ tr("Search...") }}' />
                </div>
            </div>
            <div id='sidebar-empty'>{{ tr("No results") }}</div>
            <div id='sidebar-results' class='list-group'></div>
            <div class='root list-group'>{!! $leftMenu !!}</div>
        </div>
        @endif

        @if ($rightMenu)
        <div class='sidebar right'>
            {!! $rightMenu !!}
        </div>
        @endif

        <div id='content'>

            @if ($breadcrumbs)
                <ol id='breadcrumbs' class='breadcrumb'>
                    {!! $breadcrumbs !!}
                </ol>
            @endif

            <div class='frame'>
                @if ($pageTitle)
                <h2 id='page-title'>{!! $pageTitle !!}</h2>
                @endif

                {!! $content !!}
            </div>

            <footer class="clearfix">
                {!! $footer !!}
            </footer>

            @if ($devDomain)
                <button type="button" class="btn btn-primary btn-xs db-queries" onclick="dialog.show('#db-queries')">DB queries</button>

                <div class="dialog full-screen" id="db-queries">
                    <div class='dialog-title bg-primary'>{{ tr("Database queries") }}</div>
                    <div class='dialog-content'>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ tr("Query") }}</th>
                                    <th>{{ tr("Parameters") }}</th>
                                    <th>{{ tr("Time") }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ( DB::getQueryLog() as $index => $query)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $query['query'] }}</td>
                                    <td>{{ join(', ', $query['bindings']) }}</td>
                                    <td>{{ $query['time'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                      </table>
                    </div>
                </div>
            @endif

        </div>
    </div>

    <div id='notifications'></div>
</body>
</html>
