<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class Preferences extends User
{
    protected $table = "users";

    public function __construct()
    {
        $this->table = Setting::get("table_users");
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setPasswordConfirmationAttribute($value)
    {
        $this->attributes['password_confirmation'] = bcrypt($value);
    }
}
