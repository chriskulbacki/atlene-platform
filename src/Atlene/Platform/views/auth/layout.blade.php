<!DOCTYPE html>
<html lang="{{ $language }}">
<head>
    <title>{{ $title }}</title>
    @include("platform::backend.head")
</head>
<body id='auth-page' class='{{ $bodyClasses }}'>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $title }}</h3>
        </div>
        <div class="panel-body">
            {!! $content !!}
        </div>
    </div>
</body>
</html>