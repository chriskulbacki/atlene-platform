<script>
    var m = moment();
    var formats = ["HH:mm", "h:mm A"];
    var text = "";

    for (var i = 0; i < formats.length; i++) {
        text +=
            "<a class='dt-format-sample' href='javascript:void(0)' onclick='$(\"#time_format\").val(\"" + formats[i] + "\")'>" +
                m.format(formats[i]) +
            "</a>"
    }

    $("#row-time_format_links .input-custom").append(text);
</script>