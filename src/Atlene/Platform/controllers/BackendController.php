<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Route;
use Input;
use Session;
use View;
use Request;
use Redirect;
use App;
use Auth;
use Form;

class BackendController extends BaseController
{
    use DispatchesCommands, ValidatesRequests;

    public function __construct()
    {
        // make sure the date and time formats are filled

        if (Auth::check()) {
            if (empty(Auth::user()->date_format)) {
                Auth::user()->date_format = Site::get("date_format");
            }

            if (empty(Auth::user()->time_format)) {
                Auth::user()->time_format = Site::get("time_format");
            }
        }

        Form::macro("dateinput", function($field, $value, $options, $attr) {
            is_array($options) || $options = [];
            array_key_exists("format", $options) || $options["format"] = Auth::user()->date_format;

            return view(
                override("platform::macro.datetimeinput"),
                ["field" => $field, "value" => $value, "options" => $options, "attr" => $attr, "type" => "date"]
            )->render();
        });

        Form::macro("timeinput", function($field, $value, $options, $attr) {
            is_array($options) || $options = [];
            array_key_exists("format", $options) || $options["format"] = Auth::user()->time_format;

            return view(
                override("platform::macro.datetimeinput"),
                ["field" => $field, "value" => $value, "options" => $options, "attr" => $attr, "type" => "time"]
            )->render();
        });

        Form::macro("datetimeinput", function($field, $value, $options, $attr) {
            is_array($options) || $options = [];
            if (!array_key_exists("format", $options)) {
                $options["format"] = Auth::user()->date_format . " " . Auth::user()->time_format;
            }

            return view(
                override("platform::macro.datetimeinput"),
                ["field" => $field, "value" => $value, "options" => $options, "attr" => $attr, "type" => "datetime"]
            )->render();
        });

        Form::macro("imageinput", function($field, $value, $record, $options, $attr) {
            is_array($options) || $options = [];

            if (!isset($options['initialPreview']) && $record->$field) {
                $options['initialPreview'] = (string)Element::img(Cdn::getResizedImageUrl($record->$field, false, 160));
            }

            isset($options['showUpload']) || $options['showUpload'] = false;
            isset($options['showCancel']) || $options['showCancel'] = false;
            isset($options['showCancel']) || $options['showCancel'] = false;
            isset($options['allowedFileTypes']) || $options['allowedFileTypes'] = ["image"];
            isset($options['allowedFileExtensions']) || $options['allowedFileExtensions'] = ["png", "jpg", "jpeg", "gif"];
            isset($options['removeIcon']) || $options['removeIcon'] = "";
            isset($options['browseIcon']) || $options['browseIcon'] = "";

            isset($options['language']) || $options['language'] = Javascript::getLibraryLocale("bootstrap-fileinput");

            return view(
                override("platform::macro.fileinput"),
                ["field" => $field, "value" => $value, "options" => $options, "attr" => $attr, "type" => "image"]
            )->render();
        });

        Form::macro("fileinput", function($field, $value, $record, $options, $attr) {
            is_array($options) || $options = [];

            isset($options['showUpload']) || $options['showUpload'] = false;
            isset($options['browseLabel']) || $options['browseLabel'] = tr("Browse");
            isset($options['removeLabel']) || $options['removeLabel'] = tr("Remove");
            isset($options['removeIcon']) || $options['removeIcon'] = "";
            isset($options['browseIcon']) || $options['browseIcon'] = "";

            return view(
                override("platform::macro.fileinput"),
                ["field" => $field, "value" => $value, "options" => $options, "attr" => $attr, "type" => "file"]
            )->render();
        });
    }

    public function getPage404()
    {
        return $this->view("platform::backend.error", ["message" => tr("Not found the page you are looking for is.")]);
    }

    public function view($view, array $variables = [])
    {
        // check if logged in: this handles both the platform pages and pages added by the developers,
        // using this instead of middleware, so developers don't have to remember to protect their admin routes

        if (!Auth::check()) {
            return Redirect::guest(Site::route("login"));
        }

        // check if route is allowed according to the user permissions

        if (!Access::isRouteAllowed(Route::currentRouteName())) {
            Backend::setBreadcrumbs();
            $content = View::make("platform::backend.error", ["message" => tr("Access rights to this page you lack.")]);
        } else {
            $content = View::make(Override::get($view), $variables);
        }

        return Request::ajax() ? (string)$content : self::layout($content);
    }

    public function layout($content)
    {
        if (!Auth::check()) {
            return Redirect::guest(Site::route("login"));
        }

        // get locales for the language switch menu item

        if (Locale::getCount()) {
            $locales = [];
            foreach (Locale::getAll() as $id => $locale) {
                if ($locale['enabled']) {
                    $locales[] = [
                        'name' => $locale['name'],
                        'url' => Site::url(Site::route("admin.dashboard"), $locale['code'])
                    ];
                }
            }
        } else {
            $locales = false;
        }

        // get color and gradient

        $backendColor = Color::ensureColorName(Auth::user()->backend_color);
        $backendGradient = Auth::user()->backend_gradient;

        // get classes for the body html element

        $bodyClasses = ["color-" . $backendColor];

        if ($backendGradient) {
            $bodyClasses[] = "gradient";
        }


        if (isset($_COOKIE['minimized_left'])) {
            $bodyClasses[] = "sidebar-left-minimized";
        }

        if (!Menu::getRightMenu() || isset($_COOKIE['minimized_right'])) {
            $bodyClasses[] = "sidebar-right-minimized";
        }

        $leftMenu = Menu::getLeftMenu()->renderItems();

        if (!$leftMenu) {
            $bodyClasses[] = "full-width";
        }

        $variables = [
            "libraryLocales" => Javascript::getLibraryLocalePaths(),
            "mainTitle" => Setting::get("backend_title"),
            "leftMenu" => $leftMenu,
            "rightMenu" => Menu::getRightMenu(),
            "topMenu" => Menu::getTopMenu()->renderItems(),
            "breadcrumbs" => Backend::getBreadcrumbs(),
            "pageTitle" => Backend::getPageTitle(),
            "user" => Auth::user(),
            "avatar" => Backend::avatar(64),
            "dashboardRoute" => Site::route("admin.dashboard"),
            "logoutRoute" => Site::route("logout"),
            "locales" => $locales,
            "currentLocale" => Locale::get(),
            "footer" => Menu::getFooter(),
            "content" => (string)$content,
            "bodyClasses" => implode(" ", $bodyClasses),
            "jsTranslations" => Locale::getJsTranslations("backend"),
            "devDomain" => Setting::isDevDomain(),
            "websiteUrl" => Site::route("/"),
            "head" => Backend::getHead(),
            "preferences" => Access::isRouteAllowed("admin.preferences.edit"),
            "preferencesRoute" => Site::route('admin.preferences.edit', Auth::user()->id),
            "colorRoute" => Site::route('admin.preferences.color'),
            "colors" => Color::getColorNames(),
            "backendColor" => $backendColor,
            "backendGradient" => $backendGradient,
        ];

        return View::make(Override::get("platform::backend.layout"), $variables);
    }
}
