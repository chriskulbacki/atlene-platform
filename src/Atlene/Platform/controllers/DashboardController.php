<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class DashboardController extends BackendController
{
    public function index()
    {
        Backend::setPageTitle(tr("Dashboard"));
        Backend::setBreadcrumbs();

        return $this->view(
            "platform::backend.dashboard",
            [
            ]
        );
    }
}
