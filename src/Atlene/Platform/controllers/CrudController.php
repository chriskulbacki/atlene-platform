<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Response;
use Redirect;
use Request;
use Input;
use Route;
use Validator;
use Session;
use File;
use Auth;

abstract class CrudController extends BackendController
{
    /**
     * These properties can be overriden directly or using their getter methods.
     */
    protected $modelName = null;
    protected $itemsPerPage = 50;
    protected $crudId = null;
    protected $listCode = [
        "above-buttons" => "",
        "below-buttons" => "",
        "below-filters" => "",
        "table-class" => "table table-hover table-condensed",
        "row-class" => "",
        "above-links" => "",
        "below-links" => "",
        "filter-class" => "crud-filter",
    ];
    protected $views = [
        "filter" => "platform::crud.filter",
        "list" => "platform::crud.index",
        "create" => "platform::crud.edit",
        "update" => "platform::crud.edit",
    ];
    protected $fields =[
        "id" => [
            "title" => "ID",
            "input" => [
                "create" => false,
                "update" => "custom",
            ],
            "list" => [
                "display" => true,
                "sort" => true,
                "defaultSort" => true,
                "defaultDesc" => false,
            ],
        ],
        "created_at" => [
            "title" => "Created",
            "input" => "text",
            "rules" => "required",
            "list" => true,
        ],
        "modified_by" => [
            "title" => "Modified",
            "input" => "text",
            "rules" => "required",
            "list" => true,
        ],
    ];
    protected $tabs = [];

    // controls whether the index button column is shown or hidden
    protected $showListButtonColumn = true;

    private $completedFields;
    private $completedTabs;
    private $validationErrors = [];
    private $listSort = null;
    private $listDesc = null;

    /**
     * Displays the item list.
     */
    public function index()
    {
        $modelName = $this->getModelName();
        $model = new $modelName;
        $count = $model->count();
        $views = $this->getViews();
        $fields = $this->getCompletedFields();
        $fields = $this->setFilterOptions($fields);


        $query = $this->setListRelations($model, $fields);
        $query = $this->setListSorting($query, $fields);
        $query = $this->setListFilters($query, $fields);
        $query = $this->setListPage($query, $count);

        $items = $query->get();

        $this->setListButtons($items);
        $paginator = $this->getListPaginator($items, $count, $this->getItemsPerPage());
        $hasFilters = false;

        foreach ($fields as $field => $properties) {
            if ($properties['filter']['type']) {
                $hasFilters = true;
                break;
            }
        }

        $this->processIndexItems($items);

        return $this->view(
            $views['list'],
            [
                "code" => $this->getListCode(),
                "topButtons" => $this->getListTopButtons($hasFilters),
                "crudId" => $this->getCrudId(),
                "fields" => $fields,
                "items" => $items,
                "paginator" => $paginator->render(),
                "sort" => $this->getListSort(),
                "desc" => $this->getListDesc(),
                "views" => $views,
                "hasFilters" => $hasFilters,
                "filterResetUrl" => $this->getActionUrl("index"),
                "showListButtonColumn" => $this->showListButtonColumn,
            ]
        );
    }

    /**
     * Get the create record form.
     */
    public function create()
    {
        $modelName = $this->getModelName();
        $model = new $modelName;
        $fields = $this->getCompletedFields();
        $fields = $this->setFieldInputs($fields);

        return $this->view(
            $this->getViews()['create'],
            [
                "action" => "create",
                "id" => false,
                "crudId" => $this->getCrudId(),
                "fields" => $fields,
                "tabs" => $this->getCompletedTabs("create"),
                "record" => $model,
                "targetUrl" => $this->getActionUrl("store"),
            ]
        );
    }

    /**
     * Save the newly created record.
     */
    public function store()
    {
        return $this->update(false);
    }

    /**
     * Get the edit record form.
     *
     * @param unknown $id
     */
    public function edit($id)
    {
        $modelName = $this->getModelName();
        $model = new $modelName;
        $record = $model->find($id);
        $fields = $this->getCompletedFields();
        $fields = $this->setFieldInputs($fields, $record);

        return $this->view(
            $this->getViews()['create'],
            [
                "action" => "update",
                "id" => $id,
                "crudId" => $this->getCrudId(),
                "fields" => $fields,
                "tabs" => $this->getCompletedTabs("update"),
                "record" => $record,
                "targetUrl" => $this->getActionUrl("update", $id),
            ]
        );
    }

    /**
     * Save edited record.
     *
     * @param unknown $id
     */
    public function update($id)
    {
        $modelName = $this->getModelName();
        $model = new $modelName;

        $action = $id ? "update" : "create";

        try {

            if (!$this->validateAction($action, $id)) {
                throw new \Exception($this->getValidationErrors()->toJson());
            }

            if ($action == "update") {
                $data = $model->find($id);
            } else {
                $modelName = $this->getModelName();
                $data = new $modelName;
            }

            foreach ($this->getCompletedFields() as $field => $properties) {

                if (empty($properties['save']) || !empty($properties['relation'])) {
                    continue;
                }

                // process file uploads (throws an exception on error)

                if ($this->processFileUpload($action, $field, $properties, $data)) {
                    continue;
                }

                // process other field types

                $value = Input::get($field);

                // don't save the password on update if empty
                if ($action == "update" && $properties['input']['update']['type'] == "password" && empty($value)) {
                    continue;
                }

                if ($properties['input'][$action]['type'] == false || !$properties['input'][$action]['save']) {
                    continue;
                } else {
                    $data->$field = $value;
                }
            }
        } catch (\Exception $e) {
            return Response::json(
                [
                    "status" => "error",
                    "errors" => $e->getMessage(),
                    "message" => tr("Some of the fields on the form have errors."),
                ]
            );
        }

        $data->save();
        $id = $data->id;
        $message = tr("Saved successfully.");

        // we're redirecting to the list page, let's flash the message to session
        if ($action == "create") {
            Session::flash("message", $message);
            $content = "";
            $redirect = $this->getActionUrl("index") . "?page=last";
        } else {
            $content = $this->edit($id);
            $redirect = "";
        }

        return Response::json(
            [
                "status" => "ok",
                "message" => $message,
                "content" => $content,
                "redirect" => $redirect,
            ]
        );
    }

    protected function processFileUpload($action, $field, $properties, $data)
    {
        $type = $properties['input'][$action]['type'];

        if (($type != "file" && $type != "image") || Input::get($field . "-changed") == "0") {
            return false;
        }

        // set the target directory based on the field type

        if ($type == "image") {
            // image: copy to storage path, it'll be copied to public on first access
            $target = Cdn::getStoragePath();
        } else {
            // file: copy directly to the public directory
            $target = Cdn::getPublicPath();
        }

        // the file is being cleared

        if (!Input::hasFile($field)) {
            $this->removeUnusedUploadedFiles($type, $target, $data, $field);
            $data->$field = null;
            return true;
        }

        // check if the file has been uploaded properly

        if (!Input::file($field)->isValid()) {
            throw new \Exception(json_encode([$field => tr("The file did not upload properly.")]));
        }

        // get size option

        if (isset($properties['input'][$action]['size'])) {
            $size = $properties['input'][$action]['size'];
        } else {
            $size = 1048576;
        }

        // get path

        if (isset($properties['input'][$action]['path'])) {
            $path = $properties['input'][$action]['path'];

            if (strpos($path, "::")) {
                $path = call_user_func($action, $field, $properties, Input::file($field));
            }
        }

        if (empty($path)) {
            $path = "files/" . Q::encodeId(Auth::id());
        }

        // get file name

        if (isset($properties['input'][$action]['file'])) {
            $file = $properties['input'][$action]['file'];

            if (strpos($file, "::")) {
                $file = call_user_func($action, $field, $properties, Input::file($field), $path);
            }
        }

        if (empty($file)) {
            $file = uniqid() . "." . Input::file($field)->getClientOriginalExtension();
        }

        // check size

        if (Input::file($field)->getSize() > $size) {
            throw new \Exception(
                json_encode(
                    [
                        $field => sprintf(
                            tr("The size of the uploaded file is too large. The allowed size is %s."),
                            Q::sizeToString($size)
                        )
                    ]
                )
            );
        }

        // make sure the target directory exists

        $targetPath = $target . "/" . $path;

        Cdn::makeDirectory($targetPath);

        // move the uploaded file

        try {
            Input::file($field)->move($targetPath, $file);
        } catch (\Exception $e) {
            throw new \Exception(json_encode([$field => tr("Cannot save the uploaded file.")]));
        }

        $this->removeUnusedUploadedFiles($type, $target, $data, $field);
        $data->$field = $path . "/" . $file;

        return true;
    }

    protected function removeUnusedUploadedFiles($type, $target, $data, $field)
    {
        // clear any remnant files from the target directory

        if (!empty($data->$field) && File::exists($target . "/" . $data->$field)) {
            File::delete($target . "/" . $data->$field);
        }

        // if it's an image, clear any remnant files from the public directory
        // we need to iterate through all the directories that specify the image manipulation parameters

        if ($type == "image" && !empty($data->$field)) {
            foreach (File::directories(Cdn::getPublicPath()) as $dir) {
                if (File::exists((string)$dir . "/" . $data->$field)) {
                    File::delete((string)$dir . "/" . $data->$field);
                }
            }
        }
    }

    /**
     * Delete record.
     *
     * @param unknown $id
     */
    public function destroy($id)
    {
        $modelName = $this->getModelName();
        $model = new $modelName;
        //$model->setPath($this->getActionUrl("index"));
        $model->destroy($id);

        return Response::json(
            [
                "status" => "ok",
                "message" => tr("Deleted successfully."),
                "content" => $this->index(),
            ]
        );
    }

    /**
     * Allows modifying items that will be displayed in the crud list. Items can also be modified using modifiers
     * in the model, but sometimes this is not possible, for example, if we specify a relation and the relation
     * column needs to be returned exactly as is, or the relation won't work. Then we can't use a modifier in the model
     * to change how the item will be displayed. We can use this function instead.
     *
     * Usage: iterate the items and set each item's property to how it's supposed to be displayed, for example:
     * $item->name = $item->name . " " . $item->email
     *
     * @param unknown $items
     */
    public function processIndexItems($items)
    {
    }

    /**
     * Returns the model name.
     */
    public function getModelName()
    {
        if (empty($this->modelName)) {
            throw new \Exception("Model name not specified.");
        }

        return $this->modelName;
    }

    /**
     * Returns the number of items per page to display in the list.
     * @return number
     */
    public function getItemsPerPage()
    {
        if (empty($this->itemsPerPage)) {
            $this->itemsPerPage = 50;
        }

        return $this->itemsPerPage;
    }

    public function getListPaginator($items, $count, $itemsPerPage)
    {
        // create paginator

        $paginator = new Paginator((array)$items, $count, $itemsPerPage);
        $paginator->setPath($this->getActionUrl("index"));

        // append the parameters from the url to the paginator links (for example, sort)
        $append = Input::all();
        unset($append['page']);

        return $paginator->appends($append);
    }

    public function setListPage($query, $count)
    {
        $limit = $this->getItemsPerPage();
        $pageCount = (int)ceil($count / $limit);
        $page = Input::get("page", 1);

        if ($page == "last") {
            // change the page input variable since it'll be used internally by paginator later
            Input::merge(["page" => $pageCount]);
            $page = $pageCount;
        }

         if ($page > $pageCount) {
             $page = $pageCount;
         }

        return $query->skip($limit * ($page - 1))->take($limit);
    }

    /**
     * Sets the "where" filters to query and allows for other custom query modifications.
     *
     * @param object $query
     * @param array $fields
     * @return object
     */
    public function setListFilters($query, array $fields)
    {
        foreach ($fields as $field => $properties) {
            if (!$properties['filter']['type'] || !($value = Input::get($field))) {
                continue;
            }

            if ($properties['filter']['type'] == "text") {

                $columns = explode("|", $properties['filter']['column']);

                // using advanced queries to have the fields using AND but multiple columns within a field using OR

                if ($properties['relation']) {
                    $query->whereHas($field, function($query) use ($columns, $value) {
                        $this->textFilterQuery($query, $columns, $value);
                    });
                } else {
                    $query->where(function ($query) use ($columns, $value) {
                        $this->textFilterQuery($query, $columns, $value);
                    });
                }

            } elseif ($properties['filter']['type'] == "select") {

                if ($properties['relation']) {
                     $query->whereHas($field, function($query) use ($properties, $value) {
                         $query->where($properties['filter']['column'], $value);
                     });
                } else {
                    $query->where($properties['filter']['column'], $value);
                }
            }
        }

        return $query;
    }

    private function textFilterQuery($query, $columns, $value)
    {
        $query->where($columns[0], "LIKE", "%$value%");
        for ($i = 1; $i < count($columns); $i++) {
            $query->orWhere($columns[$i], "LIKE", "%$value%");
        }
    }

    /**
     * Add lazy eager loading relations to the list query.
     *
     * @param objecct $model
     * @param array $query
     * @return mixed
     */
    public function setListRelations($query, array $fields)
    {
        foreach ($fields as $field => $properties) {
            if (!empty($properties['relation'])) {
                $query = $query->with($properties['relation']);
            }
        }

        return $query;
    }

    public function getListSort()
    {
        return $this->listSort;
    }

    public function getListDesc()
    {
        return $this->listDesc;
    }

    /**
     * Add orderBy values to the list query based on the current url parameters.
     *
     * @param object $model
     * @param array $query
     * @return object
     */
    public function setListSorting($query, array $fields)
    {
        // get sort values from the url
        $sort = Input::get("sort");
        $desc = Input::get("desc");

        // if not specified in the url, get the default sort field from the fields
        if (!$sort) {
            foreach ($fields as $field => $properties) {
                if ($properties['list']['defaultSort']) {
                    $sort = $properties['list']['defaultSort'];
                    $desc = $properties['list']['defaultDesc'];
                    break;
                }
           }
        }

        // if not specified in the fields, sort by id
        if (!$sort) {
            $sort = "id";
        }

        // check if the field specified in the url is sortable
        if (empty($fields[$sort]['list']['sort'])) {
            $sort = "id";
        }

        // store the sort values so we can pass them to the view and create a sort marker in the table head
        $this->listSort = $sort;
        $this->listDesc = $desc;

        $values = [];

        foreach (explode("|", $fields[$sort]['list']['sort']) as $val) {
            $values[$val] = $desc ? "desc" : "asc";
        }

        foreach ($values as $field => $desc) {
            $query = $query->orderBy($field, $desc);
        }

        return $query;
    }

    /**
     * Adds edit/delete buttons to each list item.
     *
     * @param object $items
     */
    public function setListButtons($items)
    {
        foreach ($items as $item) {
            $item->buttons = $this->makeListEditButton($item->id) . " " . $this->makeListDeleteButton($item->id);
        }
    }

    /**
     * Returns the buttons that will be displayed at the top of the list.
     */
    public function getListTopButtons($hasFilters = false)
    {
        $param = Q::jsParam("#crud-index-" . $this->getCrudId() . " .crud-filter-container");
        $class = Input::get("cftr") ? 'danger' : 'primary'; // if filter applied, make the button red

        $output = Element::a(
            $this->getActionUrl("create"),
            tr("Create"),
            ["class" => "btn btn-sm btn-primary submit"]
        );

        if ($hasFilters) {
            $output .= "\n" .
                Element::a(
                    false,
                    tr("Search"),
                    ["class" => "btn btn-sm btn-$class", "onclick" => "dialog.show($param)"]
                );
        }

        return $output;
    }

    /**
     * Returns the html of the default list item edit button.
     *
     * @param int $id
     */
    public function makeListEditButton($id)
    {
        return Element::a(
            $this->getActionUrl("edit", $id),
            Element::i(false, ["class" => "fa fa-pencil"]),
            [
                "class" => "btn btn-xs btn-default",
                "title" => tr("Edit"),
            ]
        );
    }

    /**
     * Returns the html of the default list item delete button.
     *
     * @param int $id
     */
    public function makeListDeleteButton($id)
    {
        $url = $this->getActionUrl("destroy", $id) . "?" . http_build_query(Input::all());

        return Element::a(
            "javascript:void(0)",
            Element::i(false, ["class" => "fa fa-times"]),
            [
                "class" => "btn btn-xs btn-default",
                "title" => tr("Delete"),
                "onclick" => "crud.destroy('$url', '" . $this->getCrudId() . "')",
            ]
        );
    }

    /**
     * Returns the crud url for the specified action and id (create/update/delete)
     *
     * @param string $action
     * @param string $id
     * @return string
     */
    public function getActionUrl($action, $param = [])
    {
        $array = explode(".", Route::currentRouteName());
        $array[count($array) - 1] = $action;

        return Site::route(implode(".", $array), $param);
    }

    /**
     * Sets an id string unique to this crud class.
     */
    public function setCrudId()
    {
        $this->crudId = crc32($this->modelName);
    }

    /**
     * Returns an id string unique to this crud class. This is useful when creating more than one crud on the page.
     *
     * @return string
     */
    public function getCrudId()
    {
        $this->crudId || $this->setCrudId();

        return $this->crudId;
    }

    /**
     * Gets the list view.
     *
     * @return string
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Gets the array of html strings to be added to the list view.
     *
     * @return array
     */
    public function getListCode()
    {
        return $this->listCode;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getTabs()
    {
        return $this->tabs;
    }

    public function getCompletedFields($runCallbacks = false)
    {
        if (!$this->completedFields) {
            $fields = $this->getFields();

            foreach ($fields as $field => $properties) {

                is_array($properties) || $properties = [];

                // title (list/create/update)
                isset($properties['title']) || $properties['title'] = [
                    "list" => $field,
                    "create" => $field,
                    "update" => $field
                ];
                is_array($properties['title']) || $properties['title'] = [
                    "list" => $properties['title'],
                    "create" => $properties['title'],
                    "update" => $properties['title'],
                ];

                // relation
                isset($properties['relation']) || $properties['relation'] = false;
                $properties['relation'] === true && ($properties['relation'] = $field);

                // input (create/update)
                array_key_exists("input", $properties) || $properties['input'] = [];

                is_array($properties['input']) || $properties['input'] = [
                    "create" => ["type" => $properties['input']],
                    "update" => ["type" => $properties['input']],
                ];

                empty($properties['input']['type']) || $properties['input'] = [
                    "create" => $properties['input'],
                    "update" => $properties['input'],
                ];

                array_key_exists("create", $properties['input']) || $properties['input']['create'] = ["type" => false];
                array_key_exists("update", $properties['input']) || $properties['input']['update'] = ["type" => false];

                is_array($properties['input']['create']) ||
                    $properties['input']['create'] = ["type" => $properties['input']['create']];
                is_array($properties['input']['update']) ||
                    $properties['input']['update'] = ["type" => $properties['input']['update']];

                array_key_exists("type", $properties['input']['create']) ||
                    $properties['input']['create']['type'] = false;
                array_key_exists("type", $properties['input']['update']) ||
                    $properties['input']['update']['type'] = false;

                array_key_exists("options", $properties['input']['create']) ||
                    $properties['input']['create']['options'] = false;
                array_key_exists("options", $properties['input']['update']) ||
                    $properties['input']['update']['options'] = false;

                array_key_exists("inline", $properties['input']['create']) ||
                    $properties['input']['create']['inline'] = true;
                array_key_exists("inline", $properties['input']['update']) ||
                    $properties['input']['update']['inline'] = true;

                array_key_exists("attr", $properties['input']['create']) ||
                    $properties['input']['create']['attr'] = [];
                array_key_exists("attr", $properties['input']['update']) ||
                    $properties['input']['update']['attr'] = [];

                array_key_exists("save", $properties['input']['create']) ||
                    $properties['input']['create']['save'] = true;
                array_key_exists("save", $properties['input']['update']) ||
                    $properties['input']['update']['save'] = true;

                // value (create only)
                isset($properties['value']) || $properties['value'] = null;

                // tab (create/update)
                isset($properties['tab']) || $properties['tab'] = "default";

                // rules (create/update)
                isset($properties['rules']) || $properties['rules'] = [
                    "create" => false,
                    "update" => false
                ];
                is_array($properties['rules']) || $properties['rules'] = [
                    "create" => $properties['rules'],
                    "update" => $properties['rules'],
                ];
                isset($properties['rules']['create']) || $properties['rules']['create'] = false;
                isset($properties['rules']['update']) || $properties['rules']['update'] = false;

                // save (set to false if will be saved manually, for example, in a relation table)
                isset($properties['save']) || $properties['save'] = true;

                // errors (create/update)
                (isset($properties['errors']) && is_array($properties['errors'])) || $properties['errors'] = [];

                // list
                isset($properties['list']) || $properties['list'] = false;
                is_array($properties['list']) || $properties['list'] = ["display" => $properties['list']];
                isset($properties['list']['sort']) || $properties['list']['sort'] = $field;
                isset($properties['list']['defaultSort']) || $properties['list']['defaultSort'] = false;
                isset($properties['list']['defaultDesc']) || $properties['list']['defaultDesc'] = false;

                // filter
                isset($properties['filter']) || $properties['filter'] = ["type" => false];
                is_array($properties['filter']) || $properties['filter'] = ["type" => $properties['filter']];
                isset($properties['filter']['options']) || $properties['filter']['options'] = [];
                isset($properties['filter']['column']) || $properties['filter']['column'] = $field;
                isset($properties['filter']['title']) || $properties['filter']['title'] = "name";
                isset($properties['filter']['attr']) || $properties['filter']['attr'] = [ "class" => "form-control"];

                $fields[$field] = $properties;
            }

            $this->completedFields = $fields;
        }

        return $this->completedFields;
    }

    public function getCompletedTabs($action)
    {
        if (!$this->completedTabs) {
            $tabs = $this->getTabs();
            is_array($tabs) || $tabs = [];
            $tabs["default"] = tr("Default");

            // ensure each tab is an array of title, active and fields

            $hasActive = false;
            foreach ($tabs as $key => $tabData) {
                is_array($tabData) || $tabs[$key] = ["title" => $tabData];
                isset($tabs[$key]['title']) || $tabs[$key]['title'] = $key;
                isset($tabs[$key]['active']) || $tabs[$key]['active'] = false;
                isset($tabs[$key]['fields']) || $tabs[$key]['fields'] = [];
                $tabs[$key]['active'] && ($hasActive = true);
            }

            if (!$hasActive) {
                reset($tabs);
                $tabs[key($tabs)]['active'] = true;
            }

            $fields = $this->getCompletedFields();

            foreach ($tabs as $key => $tabData) {
                foreach ($fields as $field => $properties) {
                    if ($properties['input'][$action]['type'] && $properties['tab'] == $key) {
                        $tabs[$key]['fields'][] = $field;
                    }
                }

                if (empty($tabs[$key]['fields'])) {
                    unset($tabs[$key]);
                }
            }

           $this->completedTabs = $tabs;
        }

        return $this->completedTabs;
    }

    /**
     * Extract rules from properties and return them.
     */
    public function getFieldRules($action)
    {
        $result = [];

        foreach ($this->getCompletedFields() as $field => $properties) {
            if ($properties['rules'][$action]) {
                $result[$field] = $properties['rules'][$action];
            }
        }

        return $result;
    }

    /**
     * Extract errors from field properties and return them.
     */
    public function getFieldErrors()
    {
        $result = [];

        foreach ($this->getCompletedFields() as $field => $properties) {
            foreach ($properties['errors'] as $key => $val) {
                $result["$field.$key"] = $val;
            }
        }

        return $result;
    }

    public function validateAction($action, $id, $data = false)
    {
        if (!$data) {
            $data = Input::all();
        }

        // add the value of 0 to any unchecked checkbox on the form so we don't get db errors on tables that
        // don't allow null fields

        $fields = $this->getCompletedFields(false);

        foreach ($fields as $key => $field) {
            if ($fields[$key]['input'][$action]['type'] == "checkbox" && !array_key_exists($key, $data)) {
                Request::merge([$key => 0]);
            }
        }

        $rules = $this->getFieldRules($action);

        foreach ($rules as $field => $rule) {
            $id && ($rules[$field] = str_replace("~id", $id, $rule));
        }

        // If updating and password fields are empty, remove them from the data. This way if the rule specifies
        // "sometimes", the field won't be validated, allowing the user to leave the password field blank if
        // it shouldn't be changed.

        if ($action == "update") {
            foreach ($this->getCompletedFields() as $field => $properties) {
                if ($properties['input']['update']['type'] == "password" &&
                    empty($data[$field]) &&
                    !empty($rules[$field])
                ) {
                    unset($data[$field]);
                }
            }
        }

        // XXX Strings::validation() should be passed a value, check how to find it, or find a different solution
        // find a way to get the error strings match the values out of the box, iterate the fields and match the
        // strings somehow
        $errorMessages = array_merge(Strings::validation(), $this->getFieldErrors());

        $validator = Validator::make($data, $rules, $errorMessages);

        if ($validator->fails()) {
            $this->validationErrors = $validator->messages();
            return false;
        }

        return true;
    }

    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Checks if there are any functions assigned as inputs in the properties and runs them to get the
     * input html that will later be placed on the form.
     *
     * @param array $fields
     * @param string $id
     * @return array
     */
    public function setFieldInputs(array $fields, $record = false)
    {
        $action = $record ? "update" : "create";

        foreach ($fields as $field => $properties) {
            if (strpos($properties['input'][$action]["type"], "::") !== false) {
                $fields[$field]['value'] = call_user_func(
                    $properties['input'][$action]["type"],
                    $record,
                    $properties['input'][$action]["options"]
                );
                $fields[$field]['input'][$action]["type"] = "custom";
            }
        }

        return $fields;
    }

    /**
     * Check if filters include any functions to be executed in order to fill their select options and run them.
     * The executed function receives $properties['filter']['column'] and "id" as parameters, which makes it
     * compatible with Query Builder's lists() method - the easiest way to get an array of id=>name from a model.
     *
     * @param array $fields
     */
    public function setFilterOptions(array $fields)
    {
        foreach ($fields as $field => $properties) {
            if (!is_array($properties['filter']['options'])) {
                // call the function to get an array of ids and titles
                $array = call_user_func($properties['filter']['options'], $properties['filter']['title'], "id")->all();
                // prepend "select" to the array (without reindexing the keys)
                $array = array_reverse($array, true);
                $array[0] = "- " . tr("Select") . " -";
                $fields[$field]['filter']['options'] = $array = array_reverse($array, true);
            }
        }

        return $fields;
    }

}
