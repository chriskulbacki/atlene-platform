<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Base class for all controllers.
 */

abstract class Controller extends \Illuminate\Routing\Controller
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * Creates the login/logout page view.
     *
     * @param string $view
     * @param unknown $variables
     */
    public static function view($view, $variables)
    {
        // get color and gradient

        $bodyClasses = ["color-" . Color::getDefaultColor()];

        if (Color::getDefaultGradient()) {
            $bodyClasses[] = "gradient";
        }

        return view(
            override("platform::auth.layout"),
            [
                "head" => Backend::getHead(),
                "language" => Locale::get("code"),
                "title" => $variables['title'],
                "bodyClasses" => implode(" ", $bodyClasses),
                "content" => view(override($view), $variables)->render(),
                "jsTranslations" => "",//Asset::getJsTranslations(),
            ]
        );
    }


}
