<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

class Strings
{
    public static function validation($type = "numeric")
    {
        $result = [
            "has_lowercase" => tr("At least one lowercase character is required."),
            "has_uppercase" => tr("At least one uppercase character is required."),
            "has_digit" => tr("At least one digit is required."),
            "has_nonalpha" => tr("At least one non-alphanumeric character is required."),
            "complex_password" => tr("The password must be at least 10 characters long and must include the following ".
                "characters: lowercase, uppercase, digit and non-alphanumeric."),

            "accepted"             => tr("This must be accepted."),
            "active_url"           => tr("This value is not a valid URL."),
            "after"                => tr("This value must contain a date after :date."),
            "alpha"                => tr("This value may only contain letters."),
            "alpha_dash"           => tr("This value may only contain letters, numbers, and dashes."),
            "alpha_num"            => tr("This value may only contain letters and numbers."),
            "array"                => tr("This value must be an array."),
            "before"               => tr("This value must contain a date before :date."),
            "boolean"              => tr("This value must be true or false."),
            "confirmed"            => tr("The confirmation does not match."),
            "date"                 => tr("This value is not a valid date."),
            "date_format"          => tr("This value does not match the format :format."),
            "different"            => tr("This and :other must be different."),
            "digits"               => tr("This value must be :digits digits."),
            "digits_between"       => tr("This value must be between :min and :max digits."),
            "email"                => tr("This value must be a valid email address."),
            "exists"               => tr("The selected :attribute is invalid."),
            "image"                => tr("This must be an image."),
            "in"                   => tr("The selected :attribute is invalid."),
            "integer"              => tr("This value must be an integer."),
            "ip"                   => tr("This value must be a valid IP address."),
            "mimes"                => tr("This value must be a file of type: :values."),
            "not_in"               => tr("The selected :attribute is invalid."),
            "numeric"              => tr("This value must be a number."),
            "regex"                => tr("This format is invalid."),
            "required"             => tr("This field is required."),
            "required_if"          => tr("This field is required when :other is :value."),
            "required_with"        => tr("This field is required when :values is present."),
            "required_with_all"    => tr("This field is required when :values is present."),
            "required_without"     => tr("This field is required when :values is not present."),
            "required_without_all" => tr("This field is required when none of :values are present."),
            "same"                 => tr("This value and :other must match."),
            "unique"               => tr("This value has already been taken."),
            "url"                  => tr("The format of this value is invalid."),
            "timezone"             => tr("This value must be a valid zone."),
        ];

        switch ($type) {
            case "file":
                $result['between'] = "The :attribute must be between :min and :max kilobytes.";
                $result['max'] = "The :attribute may not be greater than :max kilobytes.";
                $result['min'] = "The :attribute must be at least :min kilobytes.";
                $result['size'] = "The :attribute must be :size kilobytes.";
                break;
            case "string":
                $result['between'] = "The :attribute must be between :min and :max characters.";
                $result['max'] = "The :attribute may not be greater than :max characters.";
                $result['min'] = "The :attribute must be at least :min characters.";
                $result['size'] = "The :attribute must be :size characters.";
                break;
            case "array":
                $result['between'] = "The :attribute must have between :min and :max items.";
                $result['max'] = "The :attribute may not have more than :max items.";
                $result['min'] = "The :attribute must have at least :min items.";
                $result['size'] = "The :attribute must contain :size items.";
                break;
            default:
                $result["between"] = "The :attribute must be between :min and :max.";
                $result['max'] = "The :attribute may not be greater than :max.";
                $result['min'] = "The :attribute must be at least :min.";
                $result['size'] = "The :attribute must be :size.";
        }

        return $result;
    }

}
