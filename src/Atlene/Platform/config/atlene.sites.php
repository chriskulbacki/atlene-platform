<?php

return [

    1 => [
        "domains" => ["localhost"],
        "title" => "Website Name",
        "country_name" => _("United States"),
        "locale_code" => "en",
        "date_format" => "YYYY-MM-DD",
        "time_format" => "HH:mm",
        "main_currency_code" => "USD",
        "enabled_currency_codes" => "all", // or specify it this way: array("EUR", "PLN", "CZK", "HUF"),
    ],

];
