<?php namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Request;
use Route;

class MenuCode extends Model
{
    private $activeMenuSet = false;
    private $leftMenu = false;
    private $rightMenu = false;
    private $footer = false;

    public function __construct()
    {
        $this->leftMenu = new SideMenuItem(false, "root");
        $this->topMenu = new TopMenuItem(false, "root");
    }

    public function getLeftMenu()
    {
        $breadcrumbs = [];
        if ($this->leftMenu->setActive($breadcrumbs)) {

            // if breadcrumbs not set manually, set it according to the side menu
            if (Backend::getBreadcrumbs() === null) {
                Backend::setBreadcrumbs(array_reverse($breadcrumbs, true));
            }

            $this->activeMenuSet = true;
        }

        // mark all the root items so we have more options for styling

        foreach ($this->leftMenu->getItems() as $item) {
            $item->setRootItem(true);
        }

        return $this->leftMenu;
    }

    public function getTopMenu()
    {
        return $this->topMenu;
    }

    public function getRightMenu()
    {
        return $this->rightMenu;
    }

    public function setRightMenu($html)
    {
        $this->rightMenu = $html;
    }

    public function getFooter()
    {
        return $this->footer;
    }

    public function setFooter($html)
    {
        $this->footer = $html;
    }

    public function isActiveMenuSet()
    {
        return $this->activeMenuSet;
    }

    public function addSideMenuItem(SideMenuItem $item)
    {
        $this->leftMenu->add($item);
        return $item;
    }

    public function removeSideMenuItem($route)
    {
        $this->leftMenu->removeRoute($route);
    }

    public function findSideMenuItem($route)
    {
        return $this->leftMenu->find($route);
    }

    public function addTopMenuItem(TopMenuItem $item)
    {
        $this->topMenu->add($item);
        return $item;
    }

    public function findTopMenuItem($route)
    {
        return $this->topMenu->getSubItem($route);
    }

    public function removeTopMenuItem($route)
    {
        $this->topMenu->removeRoute($route);
    }
}
