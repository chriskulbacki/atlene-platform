<?php
    // http://plugins.krajee.com/file-input
    $id = rand(0, 999999);
?>

<input type='file' name='{{ $field }}' id='fileinput-{{ $id }}' />
<input type='hidden' name='{{ $field }}-changed' id='{{ $field }}-changed' value='0' />

<script>
    $("#fileinput-{{ $id }}")
        .fileinput({!! json_encode($options) !!})
        .on("fileloaded", function(event) {
            $("#{{ $field }}-changed").val(1);
        })
        .on("fileclear", function(event) {
            $("#{{ $field }}-changed").val(1);
        });
</script>