<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

/**
 * This file enables automatic url localization. When building a multilingual site, this file should be included at the
 * top of bootstrap/app.php:
 *
 * include(__DIR__ . "/../vendor/atlene/platform/src/Atlene/Platform/app.php");
 *
 * When using this functionality, the language is passed on via the url this way: http://site.com/en/route/parts.
 * After the language is recognized to exist in the url, it is applied and removed from $_SERVER['REQUEST_URI'] before
 * laravel loads it into its Request singleton. This way Laravel doesn't see the language part when processing all its
 * routes and it's not necessary to include it anywhere. It just works automatically. This processing of the request
 * needs to be done before Laravel starts up.
 */

include(__DIR__ . "/models/Configuration.php");
include(__DIR__ . "/models/LocaleCode.php");
\Atlene\Platform\LocaleCode::setLocaleFromUrl();