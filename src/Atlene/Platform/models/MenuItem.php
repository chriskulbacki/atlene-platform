<?php namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Route;

abstract class MenuItem
{
    protected $title;
    protected $route;
    protected $routeParameters;
    protected $permission;
    protected $icon;
    protected $weight;
    protected $items = [];
    protected $active = false;
    protected $hidden = false;
    protected $class = false;
    protected $target = false;
    protected $html = false;
    protected $rootItem = false;

    public static $weightCounter = 1000;

    public function __construct($title, $route, $routeParameters = [])
    {
        $this->setRoute($route);
        $this->setRouteParameters($routeParameters);
        $this->setTitle($title);
        $this->setWeight(self::$weightCounter++);
        $this->setPermission($route);
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setHtml($html)
    {
        $this->html = trim($html);
        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setRoute($route)
    {
        $this->route = trim($route);
        return $this;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setRouteParameters($routeParameters)
    {
        $this->routeParameters = $routeParameters;
        return $this;
    }

    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    public function setTitle($title)
    {
        $this->title = trim($title);
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setHidden($hidden)
    {
        $this->hidden = (bool)$hidden;
        return $this;
    }

    public function isHidden()
    {
        return $this->hidden;
    }

    public function setPermission($permission)
    {
        $this->permission = $permission;
        return $this;
    }

    public function getPermission()
    {
        return $this->permission;
    }

    public function setIcon($icon)
    {
        $this->icon = trim($icon);
        return $this;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setWeight($weight)
    {
        $this->weight = intval($weight);
        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getUrl()
    {
        try {
            if (empty($this->route)) {
                throw new \Exception();
            }
            return Site::route($this->route, $this->routeParameters);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Adds a class to the li item html element.
     **/
    public function addClass($class)
    {
        $this->class = $class;
    }

    public function setActive(array &$breadcrumbs)
    {
        // route parameters need looking into, they make it possible to add routes with parameters to the menu
        // but make it tricky if you want to set a menu active if it's something generic like crud's edit
        // then the url has a parameter, but the menu item doesn't and we can't set a parameter there because we don't
        // know which item will be edited, so we check for this below - but this is not completely tested

        $param = $this->routeParameters;

        if (empty($param) && !empty(Route::getCurrentRoute()->parameters())) {
            $param = Route::getCurrentRoute()->parameters();
        }

        if (Route::currentRouteName() == $this->route &&
            Route::getCurrentRoute()->parameters() == $param
        ) {
            $this->active = true;
            $this->target = true;
            return true;
        }

        foreach ($this->items as $item) {
            if ($item->setActive($breadcrumbs)) {
                $this->active = true;
                $breadcrumbs[$item->getTitle()] = $item->getUrl();
                return true;
            }
        }

        return false;
    }

    public function find($route)
    {
        if ($this->route == $route) {
            return $this;
        }

        foreach ($this->items as $item) {
            if ($object = $item->find($route)) {
                return $object;
            }
        }

        return false;
    }

    public function setRootItem($value)
    {
        $this->rootItem = $value;
    }

    public function isRootItem()
    {
        return $this->rootItem;
    }

    public function getSubItem($title)
    {
        foreach ($this->items as $item) {
            if ($item->getTitle() == $title) {
                return $item;
            }
        }

        return false;
    }

    public function add(MenuItem $item)
    {
        $this->items[] = $item;
        return $this;
    }

    public function renderItems()
    {
        $output = "";

        $ordered = $this->items;
        usort($ordered, function ($a, $b) {
            return $a->weight >= $b->weight;
        });

        foreach ($ordered as $item) {
            if (!$item->isHidden()) {
                $output .= $item->render($item->hasActive());
            }
        }

        return $output;
    }

    public function hasActive()
    {
        // check the current item

        if ($this->active) {
            return true;
        }

        // check all children

        foreach ($this->items as $item) {
            if ($item->hasActive()) {
                $this->active = true;
                return true;
            }
        }

        return false;
    }

    public function hasTarget()
    {
        // check the current item

        if ($this->target) {
            return true;
        }

        // check all children

        foreach ($this->items as $item) {
            if ($item->target && $item->isHidden()) {
                return true;
            }
        }

        return false;
    }

    public function removeRoute($route)
    {
        foreach ($this->items as $key => $item) {
            if ($item->getRoute() == $route) {
                unset($this->items[$key]);
                break;
            }

            $item->removeRoute($route);
        }
    }
}
