<?php namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use View;
use Request;
use Response;
use DB;
use Sentry;
use Form;
use App;
use Redirect;
use URL;
use Session;
use Exception;
use Auth;

class BackendCode
{
    private $pageTitle = null;
    private $breadcrumbs = null;
    private $visible = false;
    private $head = [];

    public function setPageTitle($title)
    {
        $this->pageTitle = $title;
    }

    public function getPageTitle()
    {
        if ($this->pageTitle !== null) {
            return $this->pageTitle;
        }

        if (!empty($this->breadcrumbs)) {
            return array_keys($this->breadcrumbs)[count($this->breadcrumbs) - 1];
        }

        return false;
    }

    /**
     * Manually sets breadcrumbs for the page. The format is: [title => link].
     * @param array $breadcrumbs
     */
    public function setBreadcrumbs(array $breadcrumbs = [])
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    public function getBreadcrumbs()
    {
        if ($this->breadcrumbs === null) {
            return null;
        }

        if (empty($this->breadcrumbs)) {
            return false;
        }

        // remove link from the last item

        $this->breadcrumbs[array_keys($this->breadcrumbs)[count($this->breadcrumbs) - 1]] = false;

        $output = "";

        foreach ($this->breadcrumbs as $title => $link) {
            $output .= $link ? Element::li(Element::a($link, $title)) : Element::li($title);
        }

        return $output;
    }

    public function avatar($size = 32)
    {
        if (!Auth::check()) {
            return false;
        }

        if (Auth::user()->avatar) {
            return Cdn::getFitImageUrl(Auth::user()->avatar, $size, $size);
        } else {
            return "//www.gravatar.com/avatar/" . md5(strtolower(trim(Auth::user()->email))) .
                "?d=" . urlencode(asset("atlene/platform/images/avatar.png")) .
                "&s=" . $size;
        }
    }


    public function redirectToPasswordChange()
    {
        return Redirect::to(Site::route("admin.preferences"))
            ->with(
                "errorMessage",
                tr(
                    "It has been over 30 days since you changed your account password. " .
                    "You must change your password before you proceed."
                )
            );
    }

    public function setVisible($value)
    {
        $this->visible = $value;
    }

    public function isVisible()
    {
        return $this->visible;
    }

    public function addToHead($code)
    {
        $this->head[] = $code;
    }

    public function getHead()
    {
        if (empty($this->head)) {
            return false;
        }

        return implode("\n", $this->head);
    }

    /**
     * Returns the date string formatted according to the logged in user preference.
     *
     * @param unknown $date
     */
    public function date($date)
    {
        if (!$date || $date == "0000-00-00 00:00:00") {
            return false;
        }

        return date(
            Backend::dateFormatToPhp(Auth::user()->date_format),
            is_numeric($date) ? $date : strtotime($date)
        );
    }

    /**
     * Returns the time string formatted according to the logged in user preference.
     *
     * @param unknown $time
     */
    public function time($time)
    {
        if (!$time || $time == "0000-00-00 00:00:00") {
            return false;
        }

        return date(
            Backend::timeFormatToPhp(Auth::user()->time_format),
            is_numeric($time) ? $time : strtotime($time)
        );
    }

    /**
     * Returns the date and time string formatted according to the logged in user preferences.
     *
     * @param unknown $date
     */
    public function dateTime($date)
    {
        if (!$date || $date == "0000-00-00 00:00:00") {
            return false;
        }

        return date(
            Backend::dateFormatToPhp(Auth::user()->date_format) . " " .
            Backend::timeFormatToPhp(Auth::user()->time_format),
            is_numeric($date) ? $date : strtotime($date)
        );
    }

    /**
     * Converts the user time format to the php format.
     *
     * @param unknown $format
     */
    public function dateFormatToPhp($format)
    {
        $replace = [
            "dddd" => "l",
            "ddd" => "*1",
            "DD" => "d",
            "D" => "j",
            "*1" => "D",

            "MMMM" => "F",
            "MMM" => "*2",
            "MM" => "m",
            "M" => "n",
            "*2" => "M",

            "YYYY" => "Y",
            "YY" => "y",
        ];

        return str_replace(array_keys($replace), array_values($replace), $format);
    }

    /**
     * Converts the user date format to the php format.
     *
     * @param unknown $format
     */
    public function timeFormatToPhp($format)
    {
        $replace = [
            "HH" => "*1",
            "H" => "G",
            "hh" => "*2",
            "h" => "g",
            "*1" => "H",
            "*2" => "h",

            "mm" => "i",
            "ss" => "s",
        ];

        return str_replace(array_keys($replace), array_values($replace), $format);
    }
}
