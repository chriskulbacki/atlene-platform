<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */


Validator::extend('has_lowercase', function($attribute, $value, $parameters) {
    return preg_match("/[a-z]/", $value);
});
Validator::extend('has_uppercase', function($attribute, $value, $parameters) {
    return preg_match("/[A-Z]/", $value);
});
Validator::extend('has_digit', function($attribute, $value, $parameters) {
    return preg_match("/\d/", $value);
});
Validator::extend('has_nonalpha', function($attribute, $value, $parameters) {
    return preg_match("/\W/", $value);
});
Validator::extend('complex_password', function($attribute, $value, $parameters) {
    return strlen($value) >= 10 &&
        preg_match("/[a-z]/", $value) &&
        preg_match("/[A-Z]/", $value) &&
        preg_match("/\d/", $value) &&
        preg_match("/\W/", $value);
});
