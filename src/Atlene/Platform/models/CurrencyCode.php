<?php
namespace Atlene\Platform;

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

use Session;
use Input;

class CurrencyCode extends \Atlene\Platform\Configuration
{
    protected $name = "atlene.currencies";

    public function setCurrent()
    {

//         // get current currency id

//         foreach ($this->currencies as $key => $val) {
//             if ($val['code'] == $this->sites[$this->siteId]["main_currency_code"]) {
//                 $this->mainCurrencyId = $key;
//                 break;
//             }
//         }

//         // get enabled currencies for this site

//         if (is_array($this->sites[$this->siteId]['enabled_currency_codes'])) {
//             foreach ($this->currencies as $key => $val) {
//                 if (in_array($val[0], $this->sites[$this->siteId]['enabled_currency_codes'])) {
//                     $this->enabledCurrencies[] = $key;
//                 }
//             }
//         } else {
//             // add all currencies
//             foreach ($this->currencies as $key => $val) {
//                 $this->enabledCurrencies[] = $key;
//             }
//         }
    }

    public function fixConfig()
    {
        $this->removeNonNumericKeys();

        foreach ($this->config as $key => $options) {
            array_key_exists("code", $options) || $this->config[$key]['code'] = "USD";
            array_key_exists("symbol", $options) || $this->config[$key]['symbol'] = "$";
            array_key_exists("name", $options) || $this->config[$key]['name'] = "U.S. Dollar";
            array_key_exists("exchange_precision", $options) || $this->config[$key]['exchange_precision'] = 0.2;
        }
    }
}
