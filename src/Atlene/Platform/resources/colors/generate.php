<?php

/**
 * @author Chris Kulbacki (http://chriskulbacki.com)
 * @copyright (c) 2015 Atlene.com
 * @license GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * @package Atlene\Platform
 */

include(__DIR__ . "/../../models/ColorCode.php");

$colorCode = new \Atlene\Platform\ColorCode();
$colorCode->generateStyles();
